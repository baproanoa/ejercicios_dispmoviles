package com.androidrecipes.smsprovider;

import android.content.AsyncTaskLoader;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.Telephony.Mms;
import android.provider.Telephony.MmsSms;
import android.provider.Telephony.Sms;
import android.provider.Telephony.Sms.Conversations;
import android.telephony.TelephonyManager;

import java.util.List;

public class ConversationLoader extends AsyncTaskLoader<List<MessageItem>> {

    public static final String[] PROJECTION = new String[]{
           
// Determinar si el mensaje es SMS o MMS
            MmsSms.TYPE_DISCRIMINATOR_COLUMN,
            //Base item ID
            BaseColumns._ID,
            //Conversation (thread) ID
            Conversations.THREAD_ID,
            //Date values
            Sms.DATE,
            Sms.DATE_SENT,
            // solo para mensajes de tipo SMS
            Sms.ADDRESS,
            Sms.BODY,
            Sms.TYPE,
            // solo para mensajes de tipo MMS
            Mms.SUBJECT,
            Mms.MESSAGE_BOX
    };

// ID del hilo de la conversación que estamos cargando

    private long mThreadId;
   
    private String mDeviceNumber;

    public ConversationLoader(Context context) {
        this(context, -1);
    }

    public ConversationLoader(Context context, long threadId) {
        super(context);
        mThreadId = threadId;
     
// Obtener el número de teléfono de este dispositivo, si está disponible
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        mDeviceNumber = manager.getLine1Number();
    }

    @Override
    protected void onStartLoading() {
    
// Recargar en cada solicitud de inicio
        forceLoad();
    }

    @Override
    public List<MessageItem> loadInBackground() {
        Uri uri;
        String[] projection;
        if (mThreadId < 0) {
            
// Cargar todas las conversaciones
            uri = MmsSms.CONTENT_CONVERSATIONS_URI;
            projection = null;
        } else {
// Cargar solo el hilo solicitado
            uri = ContentUris.withAppendedId(MmsSms.CONTENT_CONVERSATIONS_URI, mThreadId);
            projection = PROJECTION;
        }

        Cursor cursor = getContext().getContentResolver().query(
                uri,
                projection,
                null,
                null,
                null);

        return MessageItem.parseMessages(getContext(), cursor, mDeviceNumber);
    }
}
