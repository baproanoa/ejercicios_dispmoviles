package com.androidrecipes.taskstack;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.TextView;

@SuppressLint("NewApi")
public class DetailsActivity extends Activity {
  
// Cadena de acción personalizada para lanzamientos de actividades externas
    public static final String ACTION_NEW_ARRIVAL =
            "com.examples.taskstack.ACTION_NEW_ARRIVAL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
// Habilitar el botón de inicio de ActionBar con la flecha hacia arriba
        getActionBar().setDisplayHomeAsUpEnabled(true);

        TextView text = new TextView(this);
        text.setGravity(Gravity.CENTER);
        String item = getIntent().getStringExtra(Intent.EXTRA_TEXT);
        text.setText(item);

        setContentView(text);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
               
// Crea una intención para la actividad principal
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                
// Verifica si necesitamos crear la pila completa
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                  
// Esta pila aún no existe, por lo que debe sintetizarse
                    TaskStackBuilder.create(this)
                            .addParentStack(this)
                            .startActivities();
                } else {
                
// La pila existe, así que navega hacia arriba
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
