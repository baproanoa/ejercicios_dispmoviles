package com.androidrecipes.alarms;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class WorkerService extends JobService {

    private static final int MSG_JOB = 1;

    //Manejador de cola simple para ejecutar los trabajos que están programados
    private Handler mJobProcessor = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            JobParameters params = (JobParameters) msg.obj;
            Log.i("WorkerService", "Executing Job " + params.getJobId());
            //Después de completar nuestro trabajo asincrónico, debemos activar
            // jobFinished () para permitir que se ejecute la siguiente tarea programada.
            doWork();
            jobFinished(params, false);

            return true;
        }
    });

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        Log.d("WorkerService", "Start Job " + jobParameters.getJobId());
        //Para simular una tarea larga, retrasamos la ejecución en 7.5 segundos
        mJobProcessor.sendMessageDelayed(
                Message.obtain(mJobProcessor, MSG_JOB, jobParameters),
                7500
        );

        /*
         * Devuelve falso si el trabajo se completó sincrónicamente aquí,
         * verdadero si necesita hacer más trabajo en segundo plano. En lo ultimo
         * caso, debe llamar a jobFinished () para notificar al sistema de
         * finalización.
         */
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        Log.w("WorkerService", "Stop Job " + jobParameters.getJobId());
        //Cuando llega una solicitud para detener, tenemos que cancelar cualquier trabajo pendiente
        mJobProcessor.removeMessages(MSG_JOB);

        /*
         * Devuelva verdadero para reprogramar el trabajo, falso para descartarlo
         */
        return false;
    }

    private void doWork() {
        //Realiza una operación interesante, solo mostraremos la hora actual
        Calendar now = Calendar.getInstance();
        DateFormat formatter = SimpleDateFormat.getTimeInstance();
        Toast.makeText(this, formatter.format(now.getTime()), Toast.LENGTH_SHORT).show();
    }
}
