package com.androidrecipes.appwidget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;

public class SimpleAppWidget extends AppWidgetProvider {

 
/ *
     * Este método se llama para actualizar los widgets creados por este proveedor.
     * Normalmente, esto se llamará:
     * 1. Inicialmente cuando se crea el widget
     * 2. Cuando caduca el updatePeriodMillis definido en AppWidgetProviderInfo
     * /
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        
// Inicie el servicio en segundo plano para actualizar el widget
        context.startService(new Intent(context, RandomService.class));
    }
}
