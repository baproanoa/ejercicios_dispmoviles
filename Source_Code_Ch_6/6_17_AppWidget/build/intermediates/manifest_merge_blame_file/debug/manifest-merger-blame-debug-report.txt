1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.androidrecipes.appwidget"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="11"
8-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml
9        android:targetSdkVersion="24" />
9-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml
10
11    <application
11-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:6:5-46:19
12        android:debuggable="true"
13        android:icon="@drawable/ic_launcher"
13-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:8:9-45
14        android:label="@string/app_name"
14-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:7:9-41
15        android:testOnly="true"
16        android:theme="@style/AppTheme" >
16-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:9:9-40
17
18        <!-- Simple AppWidget Components -->
19        <activity android:name="com.androidrecipes.appwidget.MainActivity" >
19-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:11:9-16:20
19-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:11:19-47
20            <intent-filter>
20-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:12:13-15:29
21                <action android:name="android.intent.action.MAIN" />
21-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:13:17-68
21-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:13:25-66
22
23                <category android:name="android.intent.category.LAUNCHER" />
23-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:14:17-76
23-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:14:27-74
24            </intent-filter>
25        </activity>
26
27        <receiver android:name="com.androidrecipes.appwidget.SimpleAppWidget" >
27-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:17:9-24:20
27-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:17:19-50
28            <intent-filter>
28-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:18:13-20:29
29                <action android:name="android.appwidget.action.APPWIDGET_UPDATE" />
29-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:19:17-83
29-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:19:25-81
30            </intent-filter>
31
32            <meta-data
32-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:21:13-23:59
33                android:name="android.appwidget.provider"
33-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:22:17-58
34                android:resource="@xml/simple_appwidget" />
34-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:23:17-57
35        </receiver>
36
37        <service android:name="com.androidrecipes.appwidget.RandomService" />
37-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:25:9-49
37-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:25:18-47
38
39        <!-- Collection AppWidget Components -->
40        <activity android:name="com.androidrecipes.appwidget.ListWidgetConfigureActivity" >
40-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:28:9-32:20
40-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:28:19-62
41            <intent-filter>
41-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:29:13-31:29
42                <action android:name="android.appwidget.action.APPWIDGET_CONFIGURE" />
42-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:30:17-86
42-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:30:25-84
43            </intent-filter>
44        </activity>
45
46        <receiver android:name="com.androidrecipes.appwidget.ListAppWidget" >
46-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:33:9-40:20
46-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:33:19-48
47            <intent-filter>
47-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:18:13-20:29
48                <action android:name="android.appwidget.action.APPWIDGET_UPDATE" />
48-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:19:17-83
48-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:19:25-81
49            </intent-filter>
50
51            <meta-data
51-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:21:13-23:59
52                android:name="android.appwidget.provider"
52-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:22:17-58
53                android:resource="@xml/list_appwidget" />
53-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:23:17-57
54        </receiver>
55
56        <service
56-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:42:9-44:71
57            android:name="com.androidrecipes.appwidget.ListWidgetService"
57-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:43:13-46
58            android:permission="android.permission.BIND_REMOTEVIEWS" />
58-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:44:13-69
59        <service android:name="com.androidrecipes.appwidget.MediaService" />
59-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:45:9-48
59-->C:\Dispositivos_Proyectos\Source_Code_Ch_6\6_17_AppWidget\src\main\AndroidManifest.xml:45:18-46
60    </application>
61
62</manifest>
