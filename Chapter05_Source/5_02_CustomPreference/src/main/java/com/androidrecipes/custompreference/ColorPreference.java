package com.androidrecipes.custompreference;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.preference.DialogPreference;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;

public class ColorPreference extends DialogPreference {

    private static final int DEFAULT_COLOR = Color.WHITE;
    
/ * Copia local de la configuración de color actual * /
    private int mCurrentColor;

/ * Deslizadores para establecer componentes de color * /
    private SeekBar mRedLevel, mGreenLevel, mBlueLevel;

    public ColorPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

/ *
     * Llamado para construir un nuevo diálogo para mostrar cuando la preferencia
     * se hace clic. Creamos y configuramos una nueva vista de contenido para
     * cada instancia.
     * /
    @Override
    protected void onPrepareDialogBuilder(Builder builder) {
      
// Crea la vista de contenido del diálogo
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.preference_color, null);
        mRedLevel = (SeekBar) rootView.findViewById(R.id.selector_red);
        mGreenLevel = (SeekBar) rootView.findViewById(R.id.selector_green);
        mBlueLevel = (SeekBar) rootView.findViewById(R.id.selector_blue);

        mRedLevel.setProgress(Color.red(mCurrentColor));
        mGreenLevel.setProgress(Color.green(mCurrentColor));
        mBlueLevel.setProgress(Color.blue(mCurrentColor));

    
// Adjuntar la vista de contenido
        builder.setView(rootView);
        super.onPrepareDialogBuilder(builder);
    }

    
/ *
     * Llamado cuando el diálogo se cierra con el resultado de
     * el botón tocado por el usuario.
     * /
    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
          
// Cuando se presiona OK, obtenga y guarde el valor del color
            int color = Color.rgb(
                    mRedLevel.getProgress(),
                    mGreenLevel.getProgress(),
                    mBlueLevel.getProgress());
            setCurrentValue(color);
        }
    }

/ *
     * Llamado por el marco para obtener el valor predeterminado
     * pasado en la definición XML de preferencias
     * /
    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        
// Devuelve el valor predeterminado de XML como un color int
        ColorStateList value = a.getColorStateList(index);
        if (value == null) {
            return DEFAULT_COLOR;
        }
        return value.getDefaultColor();
    }

    /*
     * Called by the framework to set the initial value of the
     * preference, either from it's default or the last persisted
     * value.
     */
    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        setCurrentValue(restorePersistedValue ? getPersistedInt(DEFAULT_COLOR) : (Integer) defaultValue);
    }

/ *
     * Devuelve un resumen personalizado basado en la configuración actual
     * /
    @Override
    public CharSequence getSummary() {
  
// Construye el resumen con el valor del color en hexadecimal
        int color = getPersistedInt(DEFAULT_COLOR);
        String content = String.format("Current Value is 0x%02X%02X%02X",
                Color.red(color), Color.green(color), Color.blue(color));
     
// Devuelve el texto de resumen como Spannable, coloreado por la selección
        Spannable summary = new SpannableString(content);
        summary.setSpan(new ForegroundColorSpan(color), 0, summary.length(), 0);
        return summary;
    }

    private void setCurrentValue(int value) {
       
// Actualizar el último valor
        mCurrentColor = value;

        
// Guardar nuevo valor
        persistInt(value);
        // Notificar a los oyentes de preferencias
        notifyDependencyChange(shouldDisableDependents());
        notifyChanged();
    }

}
