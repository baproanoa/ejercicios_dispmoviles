package com.androidrecipes.sharedocuments;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Point;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract.Document;
import android.provider.DocumentsContract.Root;
import android.provider.DocumentsProvider;
import android.util.ArrayMap;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

public class ImageProvider extends DocumentsProvider {
    private static final String TAG = "ImageProvider";

    / * Selección reciente en caché * /
    private static String sLastDocumentId;

    private static final String DOCID_ROOT = "root:";
    private static final String DOCID_ICONS_DIR = DOCID_ROOT + "icons:";
    private static final String DOCID_BGS_DIR = DOCID_ROOT + "backgrounds:";

    
/ * Proyección predeterminada para una raíz cuando no se proporciona ninguna * /
    private static final String[] DEFAULT_ROOT_PROJECTION = {
            Root.COLUMN_ROOT_ID, Root.COLUMN_MIME_TYPES,
            Root.COLUMN_FLAGS, Root.COLUMN_ICON, Root.COLUMN_TITLE,
            Root.COLUMN_SUMMARY, Root.COLUMN_DOCUMENT_ID,
            Root.COLUMN_AVAILABLE_BYTES
    };
    
/ * Proyección predeterminada para documentos cuando no se proporciona ninguno * /
    private static final String[] DEFAULT_DOCUMENT_PROJECTION = {
            Document.COLUMN_DOCUMENT_ID, Document.COLUMN_MIME_TYPE,
            Document.COLUMN_DISPLAY_NAME, Document.COLUMN_LAST_MODIFIED,
            Document.COLUMN_FLAGS, Document.COLUMN_SIZE
    };

    private ArrayMap<String, String> mIcons;
    private ArrayMap<String, String> mBackgrounds;

    @Override
    public boolean onCreate() {
        
// Datos ficticios para nuestros documentos
        mIcons = new ArrayMap<String, String>();
        mIcons.put("logo1.png", "John Doe");
        mIcons.put("logo2.png", "Jane Doe");
        mIcons.put("logo3.png", "Jill Doe");
        mBackgrounds = new ArrayMap<String, String>();
        mBackgrounds.put("background.jpg", "Wavy Grass");

// Volcar imágenes de activos en el almacenamiento interno
        writeAssets(mIcons.keySet());
        writeAssets(mBackgrounds.keySet());
        return true;
    }

    
/ *
     * Método auxiliar para transmitir algunos archivos ficticios al
     * directorio de almacenamiento interno
     * /
    private void writeAssets(Set<String> filenames) {
        for (String name : filenames) {
            try {
                Log.d("ImageProvider", "Writing " + name + " to storage");
                InputStream in = getContext().getAssets().open(name);
                FileOutputStream out = getContext().openFileOutput(name, Context.MODE_PRIVATE);

                int size;
                byte[] buffer = new byte[1024];
                while ((size = in.read(buffer, 0, 1024)) >= 0) {
                    out.write(buffer, 0, size);
                }
                out.flush();
                out.close();
            } catch (IOException e) {
                Log.w(TAG, e);
            }
        }
    }

  
/ * Métodos auxiliares para construir documentId a partir de un nombre de archivo * /
    private String getIconsDocumentId(String filename) {
        return DOCID_ICONS_DIR + filename;
    }

    private String getBackgroundsDocumentId(String filename) {
        return DOCID_BGS_DIR + filename;
    }

   
/ * Métodos auxiliares para determinar los tipos de documentos * /
    private boolean isRoot(String documentId) {
        return DOCID_ROOT.equals(documentId);
    }

    private boolean isIconsDir(String documentId) {
        return DOCID_ICONS_DIR.equals(documentId);
    }

    private boolean isBackgroundsDir(String documentId) {
        return DOCID_BGS_DIR.equals(documentId);
    }

    private boolean isIconDocument(String documentId) {
        return documentId.startsWith(DOCID_ICONS_DIR);
    }

    private boolean isBackgroundsDocument(String documentId) {
        return documentId.startsWith(DOCID_BGS_DIR);
    }

    
/ *
     * Método auxiliar para extraer el nombre del archivo de un documentId.
     * Devuelve una cadena vacía para el documento "raíz".
     * /
    private String getFilename(String documentId) {
        int split = documentId.lastIndexOf(":");
        if (split < 0) {
            return "";
        }
        return documentId.substring(split + 1);
    }

   
/ *
     * Llamado por el sistema para determinar cuántos "proveedores" son
     * alojado aquí. Es más común devolver solo uno, a través de un
     * Cursor que tiene solo una fila de resultados.
     * /
    @Override
    public Cursor queryRoots(String[] projection) throws FileNotFoundException {
        if (projection == null) {
            projection = DEFAULT_ROOT_PROJECTION;
        }
        MatrixCursor result = new MatrixCursor(projection);
        // Agrega la raíz única para este proveedor
        MatrixCursor.RowBuilder builder = result.newRow();

        builder.add(Root.COLUMN_ROOT_ID, "root");
        builder.add(Root.COLUMN_TITLE, "Android Recipes");
        builder.add(Root.COLUMN_SUMMARY, "Android Recipes Documents Provider");
        builder.add(Root.COLUMN_ICON, R.drawable.ic_launcher);

        builder.add(Root.COLUMN_DOCUMENT_ID, DOCID_ROOT);

        builder.add(Root.COLUMN_FLAGS,
                
// Los resultados solo vendrán del sistema de archivos local
                Root.FLAG_LOCAL_ONLY
                        
// Admitimos mostrar elementos seleccionados recientemente
                        | Root.FLAG_SUPPORTS_RECENTS
                        
// Admitimos la selección del árbol de documentos (API 21+)
                        | Root.FLAG_SUPPORTS_IS_CHILD);
        builder.add(Root.COLUMN_MIME_TYPES, "image/*");
        builder.add(Root.COLUMN_AVAILABLE_BYTES, 0);

        return result;
    }

    
/ *
     * Llamado por el sistema para determinar los elementos secundarios para un determinado
     * padre. Se llamará para la raíz y para cada subdirectorio
     * definido dentro.
     * /
    @Override
    public Cursor queryChildDocuments(String parentDocumentId, String[] projection, String sortOrder)
            throws FileNotFoundException {
        if (projection == null) {
            projection = DEFAULT_DOCUMENT_PROJECTION;
        }
        MatrixCursor result = new MatrixCursor(projection);

        if (isIconsDir(parentDocumentId)) {
            // Agrega todos los archivos en la colección de iconos
            try {
                for (String key : mIcons.keySet()) {
                    addImageRow(result, mIcons.get(key), getIconsDocumentId(key));
                }
            } catch (IOException e) {
                return null;
            }
        } else if (isBackgroundsDir(parentDocumentId)) {
           
// Agrega todos los archivos en la colección de fondos
            try {
                for (String key : mBackgrounds.keySet()) {
                    addImageRow(result, mBackgrounds.get(key), getBackgroundsDocumentId(key));
                }
            } catch (IOException e) {
                return null;
            }
        } else if (isRoot(parentDocumentId)) {
            
// Agrega los directorios de nivel superior
            addIconsRow(result);
            addBackgroundsRow(result);
        }

        return result;
    }

    
/ *
     * Devuelve la misma información proporcionada a través de queryChildDocuments (), pero
     * solo para el ID de documento único solicitado.
     * /
    @Override
    public Cursor queryDocument(String documentId, String[] projection)
            throws FileNotFoundException {
        if (projection == null) {
            projection = DEFAULT_DOCUMENT_PROJECTION;
        }

        MatrixCursor result = new MatrixCursor(projection);

        try {
            String filename = getFilename(documentId);
            if (isRoot(documentId)) { //This is a query for root
                addRootRow(result);
            } else if (isIconsDir(documentId)) { //This is a query for icons
                addIconsRow(result);
            } else if (isBackgroundsDir(documentId)) { //This is a query for backgrounds
                addBackgroundsRow(result);
            } else if (isIconDocument(documentId)) {
                addImageRow(result, mIcons.get(filename), getIconsDocumentId(filename));
            } else if (isBackgroundsDocument(documentId)) {
                addImageRow(result, mBackgrounds.get(filename), getBackgroundsDocumentId(filename));
            }
        } catch (IOException e) {
            return null;
        }

        return result;
    }

   
/ *
     * Llamado para completar cualquier elemento usado recientemente de este
     * proveedor en la interfaz de usuario del selector de Recientes.
     * /
    @Override
    public Cursor queryRecentDocuments(String rootId, String[] projection)
            throws FileNotFoundException {
        if (projection == null) {
            projection = DEFAULT_DOCUMENT_PROJECTION;
        }

        MatrixCursor result = new MatrixCursor(projection);

        if (sLastDocumentId != null) {
            String filename = getFilename(sLastDocumentId);
            String recentTitle = "";
            if (isIconDocument(sLastDocumentId)) {
                recentTitle = mIcons.get(filename);
            } else if (isBackgroundsDocument(sLastDocumentId)) {
                recentTitle = mBackgrounds.get(filename);
            }

            try {
                addImageRow(result, recentTitle, sLastDocumentId);
            } catch (IOException e) {
                Log.w(TAG, e);
            }
        }
        Log.d(TAG, "Recents: " + result.getCount());
        
// Devolveremos el último resultado seleccionado a una consulta reciente
        return result;
    }

   
/ *
     * Este método es necesario SOLO si desea respaldar el documento
     * selección de árbol (API 21+). Devuelve si un documento determinado es y
     * los padres están relacionados
     * /
    @Override
    public boolean isChildDocument(String parentDocumentId, String documentId) {
        if (isRoot(parentDocumentId)) {
            // Los subdirectorios son hijos de root
            return isIconsDir(documentId)
                    || isBackgroundsDir(documentId);
        }

        if (isIconsDir(parentDocumentId)) {
            
// Todos los iconos son hijos del directorio de iconos
            return isIconDocument(documentId);
        }

        if (isBackgroundsDir(parentDocumentId)) {
            
// Todos los fondos son hijos del directorio de fondos
            return isBackgroundsDocument(documentId);
        }

      
// De lo contrario, estos identificadores no se conocen entre sí
        return false;
    }

    
/ *
     * Método auxiliar para escribir la raíz en el suministrado
     * Cursor
     * /
    private void addRootRow(MatrixCursor cursor) {
        addDirRow(cursor, DOCID_ROOT, "Root");
    }

    private void addIconsRow(MatrixCursor cursor) {
        addDirRow(cursor, DOCID_ICONS_DIR, "Icons");
    }

    private void addBackgroundsRow(MatrixCursor cursor) {
        addDirRow(cursor, DOCID_BGS_DIR, "Backgrounds");
    }

/ *
     * Método auxiliar para escribir un subdirectorio específico en
     * el Cursor suministrado
     * /

    private void addDirRow(MatrixCursor cursor, String documentId, String name) {
        final MatrixCursor.RowBuilder row = cursor.newRow();

        row.add(Document.COLUMN_DOCUMENT_ID, documentId);
        row.add(Document.COLUMN_DISPLAY_NAME, name);
        row.add(Document.COLUMN_SIZE, 0);
        row.add(Document.COLUMN_MIME_TYPE, Document.MIME_TYPE_DIR);

        long installed;
        try {
            installed = getContext().getPackageManager()
                    .getPackageInfo(getContext().getPackageName(), 0)
                    .firstInstallTime;
        } catch (NameNotFoundException e) {
            installed = 0;
        }
        row.add(Document.COLUMN_LAST_MODIFIED, installed);
        row.add(Document.COLUMN_FLAGS, 0);
    }

   
/ *
     * Método auxiliar para escribir un archivo de imagen específico en
     * el Cursor suministrado
     * /
    private void addImageRow(MatrixCursor cursor, String title, String documentId) throws IOException {
        final MatrixCursor.RowBuilder row = cursor.newRow();

        String filename = getFilename(documentId);
        AssetFileDescriptor afd = getContext().getAssets().openFd(filename);

        row.add(Document.COLUMN_DOCUMENT_ID, documentId);
        row.add(Document.COLUMN_DISPLAY_NAME, title);
        row.add(Document.COLUMN_SIZE, afd.getLength());
        row.add(Document.COLUMN_MIME_TYPE, "image/*");

        long installed;
        try {
            installed = getContext().getPackageManager()
                    .getPackageInfo(getContext().getPackageName(), 0)
                    .firstInstallTime;
        } catch (NameNotFoundException e) {
            installed = 0;
        }
        row.add(Document.COLUMN_LAST_MODIFIED, installed);
        row.add(Document.COLUMN_FLAGS, Document.FLAG_SUPPORTS_THUMBNAIL);
    }

   
/ *
     * Devuelve una referencia a un recurso de imagen que utilizará el marco.
     * en la lista de elementos de cualquier documento con FLAG_SUPPORTS_THUMBNAIL
     * bandera habilitada. Este método es seguro para bloquear mientras se descarga contenido.
     * /
    @Override
    public AssetFileDescriptor openDocumentThumbnail(String documentId, Point sizeHint, CancellationSignal signal)
            throws FileNotFoundException {
        
// Cargaremos la miniatura de la versión en almacenamiento
        String filename = getFilename(documentId);
       
// Crea una referencia de archivo a la imagen en el almacenamiento interno
        final File file = new File(getContext().getFilesDir(), filename);
      
// Devuelve un descriptor de archivo que envuelve la referencia del archivo
        final ParcelFileDescriptor pfd =
                ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
        return new AssetFileDescriptor(pfd, 0, AssetFileDescriptor.UNKNOWN_LENGTH);
    }

   
/ *
     * Devuelve un descriptor de archivo al documento al que hace referencia el
     * documentId. El cliente utilizará este descriptor para leer el contenido.
     * directamente. Este método es seguro para bloquear mientras se descarga contenido.
     * /
    @Override
    public ParcelFileDescriptor openDocument(String documentId, String mode, CancellationSignal signal)
            throws FileNotFoundException {
        
// Cargaremos el documento en sí desde los activos
        try {
            String filename = getFilename(documentId);
           
// Crea una referencia de archivo a la imagen en el almacenamiento interno
            final File file = new File(getContext().getFilesDir(), filename);
           
// Devuelve un descriptor de archivo que envuelve la referencia del archivo
            final ParcelFileDescriptor pfd =
                    ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);

            
// Guarda esto como el último documento seleccionado
            sLastDocumentId = documentId;

            return pfd;
        } catch (IOException e) {
            Log.w(TAG, e);
            return null;
        }
    }
}
