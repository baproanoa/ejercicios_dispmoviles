package com.androidrecipes.mapper;

import android.graphics.Color;
import android.location.Location;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import java.util.ArrayList;

public class ShapeAdapter implements OnMapClickListener {

    private static final float STROKE_SELECTED = 6.0f;
    private static final float STROKE_NORMAL = 2.0f;
    /* Colores para las regiones dibujadas */
    private static final int COLOR_STROKE = Color.RED;
    private static final int COLOR_FILL = Color.argb(127, 0, 0, 255);

    /*
     * Interfaz externa para notificar a los oyentes de un cambio en
     * la región seleccionada según los toques del usuario
     */
    public interface OnRegionSelectedListener {
        public void onRegionSelected(Region selectedRegion);

        public void onNoRegionSelected();
    }

    /*
     * Definición base de una región interactiva en el mapa.
     * Define métodos para cambiar la pantalla y verificar los toques del usuario
     */
    public static abstract class Region {
        private String mRegionName;

        public Region(String regionName) {
            mRegionName = regionName;
        }

        public String getName() {
            return mRegionName;
        }

        //Verifique si una ubicación está dentro de esta región
        public abstract boolean hitTest(LatLng point);

        //Cambiar la visualización de la base de la región en la selección
        public abstract void setSelected(boolean isSelected);
    }

    /*
     * Implementación de una región dibujada como un círculo
     */
    private static class CircleRegion extends Region {
        private Circle mCircle;

        public CircleRegion(String name, Circle circle) {
            super(name);
            mCircle = circle;
        }

        @Override
        public boolean hitTest(LatLng point) {
            final LatLng center = mCircle.getCenter();
            float[] result = new float[1];
            Location.distanceBetween(center.latitude, center.longitude,
                    point.latitude, point.longitude,
                    result);

            return (result[0] < mCircle.getRadius());
        }

        @Override
        public void setSelected(boolean isSelected) {
            mCircle.setStrokeWidth(isSelected ? STROKE_SELECTED : STROKE_NORMAL);
        }

    }

    /*
     * Implementación de una región dibujada como un rectángulo
     */
    private static class RectRegion extends Region {
        private Polygon mRect;
        private LatLngBounds mRectBounds;

        public RectRegion(String name, Polygon rect, LatLng southwest, LatLng northeast) {
            super(name);
            mRect = rect;
            mRectBounds = new LatLngBounds(southwest, northeast);
        }

        @Override
        public boolean hitTest(LatLng point) {
            return mRectBounds.contains(point);
        }

        @Override
        public void setSelected(boolean isSelected) {
            mRect.setStrokeWidth(isSelected ? STROKE_SELECTED : STROKE_NORMAL);
        }
    }

    private GoogleMap mMap;

    private OnRegionSelectedListener mRegionSelectedListener;
    private ArrayList<Region> mRegions;
    private Region mCurrentRegion;

    public ShapeAdapter(GoogleMap map) {
        mRegions = new ArrayList<Region>();

        mMap = map;
        mMap.setOnMapClickListener(this);
    }

    public void setOnRegionSelectedListener(OnRegionSelectedListener listener) {
        mRegionSelectedListener = listener;
    }

    /*
     * Construya y agregue una nueva región circular alrededor del punto dado.
     */
    public void addCircularRegion(String name, LatLng center, double radius) {
        CircleOptions options = new CircleOptions()
                .center(center)
                .radius(radius);
        options.strokeWidth(STROKE_NORMAL).strokeColor(COLOR_STROKE).fillColor(COLOR_FILL);

        Circle c = mMap.addCircle(options);
        mRegions.add(new CircleRegion(name, c));
    }

    /*
     * Construya y agregue una nueva región rectangular alrededor con los límites dados
     */
    public void addRectangularRegion(String name, LatLng southwest, LatLng northeast) {
        PolygonOptions options = new PolygonOptions().add(
                new LatLng(southwest.latitude, southwest.longitude),
                new LatLng(southwest.latitude, northeast.longitude),
                new LatLng(northeast.latitude, northeast.longitude),
                new LatLng(northeast.latitude, southwest.longitude));
        options.strokeWidth(STROKE_NORMAL).strokeColor(COLOR_STROKE).fillColor(COLOR_FILL);

        Polygon p = mMap.addPolygon(options);
        mRegions.add(new RectRegion(name, p, southwest, northeast));
    }

    /*
     * Manejar eventos de toque entrantes desde el objeto de mapa.
     * Determine qué elemento de región puede haber sido seleccionado. Si
     * las regiones se superponen en este punto, se seleccionará la primera agregada.
     */
    @Override
    public void onMapClick(LatLng point) {
        Region newSelection = null;
        //Busque y seleccione la región marcada
        for (Region region : mRegions) {
            if (region.hitTest(point) && newSelection == null) {
                region.setSelected(true);
                newSelection = region;
            } else {
                region.setSelected(false);
            }
        }

        if (mCurrentRegion != newSelection) {
            //Notificar y actualizar el cambio
            if (newSelection != null && mRegionSelectedListener != null) {
                mRegionSelectedListener.onRegionSelected(newSelection);
            } else if (mRegionSelectedListener != null) {
                mRegionSelectedListener.onNoRegionSelected();
            }

            mCurrentRegion = newSelection;
        }
    }

}
