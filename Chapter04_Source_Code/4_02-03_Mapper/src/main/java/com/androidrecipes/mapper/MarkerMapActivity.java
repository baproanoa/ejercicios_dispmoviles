package com.androidrecipes.mapper;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MarkerMapActivity extends FragmentActivity implements
        RadioGroup.OnCheckedChangeListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.InfoWindowAdapter, OnMapReadyCallback {
    private static final String TAG = "AndroidRecipes";
    private static final int REQUEST_CODE_PERMISSIONS = 10;

    private SupportMapFragment mMapFragment;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        //Verifica que los servicios de juego estén activos y actualizados
        int resultCode = GoogleApiAvailability.getInstance()
                .isGooglePlayServicesAvailable(this);
        switch (resultCode) {
            case ConnectionResult.SUCCESS:
                Log.d(TAG, "Google Play Services is ready to go!");
                break;
            default:
                showPlayServicesError(resultCode);
                return;
        }

        mMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        // Conecte la interfaz de usuario del selector de tipo de mapa
        RadioGroup typeSelect = (RadioGroup) findViewById(R.id.group_maptype);
        typeSelect.setEnabled(false);
        typeSelect.setOnCheckedChangeListener(this);
        typeSelect.check(R.id.type_normal);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapFragment.getMapAsync(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMap = null;
    }

    /**
     * OnCheckedChangeListener Methods
     */
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.type_satellite:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.type_normal:
            default:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
        }
    }

    /**
     * OnMarkerClickListener Methods
     */

    @Override
    public boolean onMarkerClick(Marker marker) {
        // Devuelva verdadero para deshabilitar el centro automático y la ventana emergente de información
        return false;
    }

    /**
     * OnMarkerDragListener Methods
     */

    @Override
    public void onMarkerDrag(Marker marker) {
        // Haz algo mientras el marcador se está moviendo
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        Log.i("MarkerTest", "Drag " + marker.getTitle()
                + " to " + marker.getPosition());
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        Log.d("MarkerTest", "Drag " + marker.getTitle()
                + " from " + marker.getPosition());
    }

    /**
     * OnInfoWindowClickListener Methods
     */

    @Override
    public void onInfoWindowClick(Marker marker) {
        // Actuar sobre el evento de selección, aquí simplemente cerramos la ventana
        marker.hideInfoWindow();
    }

    /**
     * InfoWindowAdapter Methods
     */

    /*
     * Devuelve una vista de contenido para colocarla dentro de una ventana de información estándar. Solamente
     * llamado si getInfoWindow () devuelve nulo.
     */
    @Override
    public View getInfoContents(Marker marker) {
        //Try returning createInfoView() here instead
        return null;
    }

    /*
     * Devuelve la ventana de información completa para que se muestre.
     */
    @Override
    public View getInfoWindow(Marker marker) {
        View content = createInfoView(marker);
        content.setBackgroundResource(R.drawable.background);
        return content;
    }

    /*
     * Método de ayuda privada para construir la vista de contenido
     */
    private View createInfoView(Marker marker) {
        // No tenemos un padre para el diseño, así que pase null
        View content = getLayoutInflater().inflate(
                R.layout.info_window, null);
        ImageView image = (ImageView) content
                .findViewById(R.id.image);
        TextView text = (TextView) content
                .findViewById(R.id.text);

        image.setImageResource(R.drawable.ic_launcher);
        text.setText(marker.getTitle());

        return content;
    }

    /*
     * Cuando Play Services falta o está en la versión incorrecta, el cliente
     * la biblioteca ayudará con un diálogo para ayudar al usuario a actualizar.
     */
    private void showPlayServicesError(int errorCode) {
        // Obtener el cuadro de diálogo de error de los servicios de Google Play
        GoogleApiAvailability.getInstance()
                .showErrorDialogFragment(this, errorCode, 1,
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                finish();
                            }
                        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Supervisar la interacción con los elementos del marcador
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMarkerDragListener(this);
        // Configurar nuestra aplicación para ofrecer vistas para las ventanas de información
        mMap.setInfoWindowAdapter(this);
        // Supervisar eventos de clic en ventanas de información
        mMap.setOnInfoWindowClickListener(this);

        // Google HQ 37.427,-122.099
        Marker marker = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.4218, -122.0840))
                .title("Google HQ")
                // Muestra un recurso de imagen de nuestra aplicación como marcador
                .icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.logo))
                //Reducir la opacidad
                .alpha(0.6f));
        //Hacer que este marcador se pueda arrastrar en el mapa
        marker.setDraggable(true);

        // Restar 0.01 grados
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.4118, -122.0740))
                .title("Neighbor #1")
                .snippet("Best Restaurant in Town")
                // Show a default marker, in the default color
                .icon(BitmapDescriptorFactory.defaultMarker()));

        //Agregue 0.01 grados
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.4318, -122.0940))
                .title("Neighbor #2")
                .snippet("Worst Restaurant in Town")
                //Mostrar un marcador predeterminado, con un tinte azul
                .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

        // Centrar y acercar el mapa simultáneamente
        LatLng mapCenter = new LatLng(37.4218, -122.0840);
        CameraUpdate newCamera = CameraUpdateFactory
                .newLatLngZoom(mapCenter, 13);
        mMap.moveCamera(newCamera);

        RadioGroup typeSelect = (RadioGroup) findViewById(R.id.group_maptype);
        typeSelect.setEnabled(true);
    }
}
