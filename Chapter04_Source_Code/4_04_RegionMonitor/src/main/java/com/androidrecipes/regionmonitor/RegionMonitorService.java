package com.androidrecipes.regionmonitor;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

public class RegionMonitorService extends Service {
    //Acción única para identificar solicitudes de inicio frente a eventos
    public static final String ACTION_INIT =
            "com.androidrecipes.regionmonitor.ACTION_INIT";
    private static final String TAG = "RegionMonitorService";
    private static final int NOTE_ID = 100;
    private NotificationManager mNoteManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mNoteManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        //Publica una notificación del sistema cuando se inicia el servicio
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setContentTitle("Geofence Service");
        builder.setContentText("Waiting for transition...");
        builder.setOngoing(true);

        Notification note = builder.build();
        mNoteManager.notify(NOTE_ID, note);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Todavía no hay nada que hacer, recién comenzando el servicio
        if (ACTION_INIT.equals(intent.getAction())) {
            //We don't care if this service dies unexpectedly
            return START_NOT_STICKY;
        }

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            //Log any errors
            Log.w(TAG, "Error monitoring region: "
                    + geofencingEvent.getErrorCode());
        } else {
            //Actualizar la notificación en curso del nuevo evento
            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(this);
            builder.setSmallIcon(R.drawable.ic_launcher);
            builder.setDefaults(Notification.DEFAULT_SOUND
                    | Notification.DEFAULT_LIGHTS);
            builder.setAutoCancel(true);

            int transitionType = geofencingEvent.getGeofenceTransition();

            //Compruebe si entramos o salimos de la región
            if (transitionType == Geofence.GEOFENCE_TRANSITION_ENTER) {
                builder.setContentTitle("Geofence Transition");
                builder.setContentText("Entered your Geofence");
            } else if (transitionType == Geofence.GEOFENCE_TRANSITION_EXIT) {
                builder.setContentTitle("Geofence Transition");
                builder.setContentText("Exited your Geofence");
            }

            Notification note = builder.build();
            mNoteManager.notify(NOTE_ID, note);
        }

        //No nos importa si este servicio muere inesperadamente
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Cuando el servicio muera, cancele nuestra notificación en curso
        mNoteManager.cancel(NOTE_ID);
    }

    /* No vinculamos a este servicio */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
