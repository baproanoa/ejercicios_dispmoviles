package com.androidrecipes.usermotionactivity;

import android.app.IntentService;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

public class UserMotionService extends IntentService {
    private static final String TAG = "UserMotionService";

    /*
     * Interfaz de devolución de llamada para cambios de tipo de actividad detectados
     */
    public interface OnActivityChangedListener {
        public void onUserActivityChanged(int bestChoice, int bestConfidence,
                                          ActivityRecognitionResult newActivity);
    }

    /* Último tipo de actividad detectada * /
    Private DetectedActivity mLastKnownActivity;
    /*
     * Marshals solicita desde el hilo de fondo para que las devoluciones de llamada
     * se puede hacer en el hilo principal (UI).
     */
    private CallbackHandler mHandler;

    private static class CallbackHandler extends Handler {
        /* Devolución de llamada para cambios de actividad */
        private OnActivityChangedListener mCallback;

        public void setCallback(OnActivityChangedListener callback) {
            mCallback = callback;
        }

        @Override
        public void handleMessage(Message msg) {
            if (mCallback != null) {
                //Leer datos de carga útil del mensaje y devolver la llamada
                ActivityRecognitionResult newActivity = (ActivityRecognitionResult) msg.obj;
                mCallback.onUserActivityChanged(msg.arg1, msg.arg2, newActivity);
            }
        }
    }

    public UserMotionService() {
        //La cadena se usa para nombrar el hilo de fondo creado
        super("UserMotionService");
        mHandler = new CallbackHandler();
    }

    public void setOnActivityChangedListener(OnActivityChangedListener listener) {
        mHandler.setCallback(listener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.w(TAG, "Service is stopping...");
    }

    /*
     * Los próximos eventos de acción del marco vendrán
     * aquí. Esto se llama en un hilo de fondo, por lo que
     * Podemos hacer un procesamiento largo aquí si lo deseamos.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        if (ActivityRecognitionResult.hasResult(intent)) {
            //Extraiga el resultado de la intención
            ActivityRecognitionResult result =
                    ActivityRecognitionResult.extractResult(intent);
            DetectedActivity activity = result.getMostProbableActivity();
            Log.v(TAG, "New User Activity Event");

            //Si la probabilidad más alta es DESCONOCIDA, pero la confianza es baja,
            // comprobar si existe otro y seleccionarlo en su lugar
            if (activity.getType() == DetectedActivity.UNKNOWN
                    && activity.getConfidence() < 60
                    && result.getProbableActivities().size() > 1) {
                //Seleccione el siguiente elemento probable
                activity = result.getProbableActivities().get(1);
            }

            //Ante un cambio de actividad, avise a la devolución de llamada
            if (mLastKnownActivity == null
                    || mLastKnownActivity.getType() != activity.getType()
                    || mLastKnownActivity.getConfidence() != activity.getConfidence()) {
                //Pasar los resultados al hilo principal dentro de un mensaje
                Message msg = Message.obtain(null,
                        0,                         //what
                        activity.getType(),        //arg1
                        activity.getConfidence(),  //arg2
                        result);
                mHandler.sendMessage(msg);
            }
            mLastKnownActivity = activity;
        }
    }

    /*
     *Esto se llama cuando la actividad quiere vincularse al
     * Servicio. Tenemos que proporcionar una envoltura alrededor de esta instancia.
     * para devolverlo.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /*
     * Este es un contenedor simple que podemos pasar a la actividad.
     * para permitirle el acceso directo a este servicio.
     */
    private LocalBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public UserMotionService getService() {
            return UserMotionService.this;
        }
    }

    /*
     * Utilidad para obtener un buen nombre para mostrar para cada estado
     */
    public static String getActivityName(DetectedActivity activity) {
        switch (activity.getType()) {
            case DetectedActivity.IN_VEHICLE:
                return "Driving";
            case DetectedActivity.ON_BICYCLE:
                return "Biking";
            case DetectedActivity.ON_FOOT:
                return "Walking";
            case DetectedActivity.STILL:
                return "Not Moving";
            case DetectedActivity.TILTING:
                return "Tilting";
            case DetectedActivity.UNKNOWN:
            default:
                return "No Clue";
        }
    }
}
