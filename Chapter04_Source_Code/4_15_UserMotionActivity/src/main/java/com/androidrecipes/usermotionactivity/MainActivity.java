package com.androidrecipes.usermotionactivity;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidrecipes.usermotionactivity.UserMotionService.LocalBinder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SupportErrorDialogFragment;
import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

public class MainActivity extends AppCompatActivity implements
        ServiceConnection,
        UserMotionService.OnActivityChangedListener,
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener {
    private static final String TAG = "UserActivity";

    private Intent mServiceIntent;
    private PendingIntent mCallbackIntent;
    private UserMotionService mService;

    private ActivityRecognitionClient mRecognitionClient;
    //Adaptador de lista personalizada para mostrar nuestros resultados
    private ActivityAdapter mListAdapter;

    private View mBlockingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBlockingView = findViewById(R.id.blocker);

        //Construya un adaptador de lista simple que muestre todos los
        //eventos de cambio de actividad entrante del servicio.
        ListView list = (ListView) findViewById(R.id.list);
        mListAdapter = new ActivityAdapter(this);
        list.setAdapter(mListAdapter);

        //Cuando se hace clic en la lista, muestra todas las actividades probables
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                showDetails(mListAdapter.getItem(position));
            }
        });

        //Verifica que los servicios de juego estén activos y actualizados
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        switch (resultCode) {
            case ConnectionResult.SUCCESS:
                Log.d(TAG, "Google Play Services is ready to go!");
                break;
            default:
                showPlayServicesError(resultCode);
                return;
        }

        //Cree una instancia de cliente para hablar con los servicios de Google
        mRecognitionClient = new ActivityRecognitionClient(this, this, this);
        //Crea una intención para vincularte al servicio
        mServiceIntent = new Intent(this, UserMotionService.class);
        //Cree un PendingIntent que los servicios de Google utilizarán para las devoluciones de llamada
        mCallbackIntent = PendingIntent.getService(this, 0,
                mServiceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Conéctese a los servicios de Google y a nuestro servicio
        mRecognitionClient.connect();
        bindService(mServiceIntent, this, BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Desconectarse de todos los servicios
        mRecognitionClient.removeActivityUpdates(mCallbackIntent);
        mRecognitionClient.disconnect();

        disconnectService();
        unbindService(this);
    }

    /**
     * ServiceConnection Methods
     */

    public void onServiceConnected(ComponentName name, IBinder service) {
        //Adjuntarnos a nuestro Servicio como devolución de llamada para eventos
        mService = ((LocalBinder) service).getService();
        mService.setOnActivityChangedListener(this);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        disconnectService();
    }

    private void disconnectService() {
        if (mService != null) {
            mService.setOnActivityChangedListener(null);
        }
        mService = null;
    }

    /**
     * Google Services Connection Callbacks
     */

    @Override
    public void onConnected(Bundle connectionHint) {
        //Debemos esperar hasta que los servicios estén conectados
        // para solicitar actualizaciones.
        mRecognitionClient.requestActivityUpdates(5000, mCallbackIntent);
    }

    @Override
    public void onDisconnected() {
        Log.w(TAG, "Google Services Disconnected");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.w(TAG, "Google Services Connection Failure");
    }

    /**
     * OnActivityChangedListener Methods
     */

    @Override
    public void onUserActivityChanged(int bestChoice, int bestConfidence,
                                      ActivityRecognitionResult newActivity) {
        //Agregar último evento a la lista
        mListAdapter.add(newActivity);
        mListAdapter.notifyDataSetChanged();

        //Determine la acción del usuario según nuestro algoritmo personalizado
        switch (bestChoice) {
            case DetectedActivity.IN_VEHICLE:
            case DetectedActivity.ON_BICYCLE:
                mBlockingView.setVisibility(View.VISIBLE);
                break;
            case DetectedActivity.ON_FOOT:
            case DetectedActivity.STILL:
                mBlockingView.setVisibility(View.GONE);
                break;
            default:
                //Ignorar otros estados
                break;
        }
    }

    /*
     * Utilidad que construye un simple brindis con todos los probables
     * opciones de actividades con sus valores de confianza
     */
    private void showDetails(ActivityRecognitionResult activity) {
        StringBuilder sb = new StringBuilder();
        sb.append("Details:");
        for (DetectedActivity element : activity.getProbableActivities()) {
            sb.append("\n" + UserMotionService.getActivityName(element)
                    + ", " + element.getConfidence() + "% sure");
        }

        Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show();
    }

    /*
     * ListAdapter para mostrar el resultado de cada actividad que recibimos del servicio
     */
    private static class ActivityAdapter extends ArrayAdapter<ActivityRecognitionResult> {

        public ActivityAdapter(Context context) {
            super(context, android.R.layout.simple_list_item_1);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext())
                        .inflate(android.R.layout.simple_list_item_1, parent, false);
            }
            //Muestra la actividad más probable con su confianza en la lista.
            TextView tv = (TextView) convertView;
            ActivityRecognitionResult result = getItem(position);
            DetectedActivity newActivity = result.getMostProbableActivity();
            String entry = DateFormat.format("hh:mm:ss", result.getTime())
                    + ": " + UserMotionService.getActivityName(newActivity) + ", "
                    + newActivity.getConfidence() + "% confidence";
            tv.setText(entry);

            return convertView;
        }
    }


    /*
     * Cuando Play Services falta o está en la versión incorrecta, el cliente
     * la biblioteca ayudará con un diálogo para ayudar al usuario a actualizar.
     */
    private void showPlayServicesError(int errorCode) {
        //Obtener el cuadro de diálogo de error de los servicios de Google Play
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                errorCode,
                this,
                1000 /* RequestCode */);
        // Si los servicios de Google Play pueden proporcionar un cuadro de diálogo de error
        if (errorDialog != null) {
            // Cree un nuevo DialogFragment para el diálogo de error
            SupportErrorDialogFragment errorFragment = SupportErrorDialogFragment.newInstance(errorDialog);
            // Mostrar el diálogo de error en DialogFragment
            errorFragment.show(
                    getSupportFragmentManager(),
                    "Activity Tracker");
        }
    }
}
