package com.androidrecipes.videooverlay;

import android.app.Activity;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.IOException;

public class VideoCaptureActivity extends Activity implements SurfaceHolder.Callback {

    private Camera mCamera;
    private MediaRecorder mRecorder;

    private SurfaceView mPreview;
    private Button mRecordButton;

    private boolean mRecording = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mRecordButton = (Button) findViewById(R.id.button_record);
        mRecordButton.setText("Start Recording");

        mPreview = (SurfaceView) findViewById(R.id.surface_video);
        mPreview.getHolder().addCallback(this);

        mCamera = Camera.open();
        //Gire la pantalla de vista previa para que coincida con el retrato
        mCamera.setDisplayOrientation(90);
        mRecorder = new MediaRecorder();
    }

    @Override
    protected void onDestroy() {
        mCamera.release();
        mCamera = null;
        super.onDestroy();
    }

    public void onRecordClick(View v) {
        updateRecordingState();
    }

    /*
     * Inicialice la cámara y la grabadora.
     * El orden de estos métodos es importante porque MediaRecorder es una
     * máquina de estado estricta que se mueve a través de estados a medida que se llaman los métodos.
     */
    private void initializeRecorder() throws IllegalStateException, IOException {
        //Desbloquee la cámara para permitir que MediaRecorder la use
        mCamera.unlock();
        mRecorder.setCamera(mCamera);
        //Actualizar la configuración de la fuente
        mRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        //Actualizar la configuración de salida
        File recordOutput = new File(Environment.getExternalStorageDirectory(), "recorded_video.mp4");
        if (recordOutput.exists()) {
            recordOutput.delete();
        }
        CamcorderProfile cpHigh = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
        mRecorder.setProfile(cpHigh);
        mRecorder.setOutputFile(recordOutput.getAbsolutePath());
        //Coloque la superficie en la grabadora para permitir la vista previa durante la grabación
        mRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());

        //Opcionalmente, establezca valores límite en la grabación
        mRecorder.setMaxDuration(50000); // 50 seconds
        mRecorder.setMaxFileSize(5000000); // Approximately 5 megabytes

        mRecorder.prepare();
    }

    private void updateRecordingState() {
        if (mRecording) {
            mRecording = false;
            //Restablecer el estado de la grabadora para la siguiente grabación
            mRecorder.stop();
            mRecorder.reset();
            //Retira la cámara para que continúe la vista previa
            mCamera.lock();
            mRecordButton.setText("Start Recording");
        } else {
            try {
                //Reinicie la grabadora para la próxima sesión
                initializeRecorder();
                //Iniciar la grabación
                mRecording = true;
                mRecorder.start();
                mRecordButton.setText("Stop Recording");
            } catch (Exception e) {
                //Se produjo un error al inicializar la grabadora
                e.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        //Cuando obtenemos una superficie, iniciamos inmediatamente la vista previa de la cámara
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }
}
