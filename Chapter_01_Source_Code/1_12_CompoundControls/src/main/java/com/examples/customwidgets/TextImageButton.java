package com.examples.customwidgets;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class TextImageButton extends FrameLayout {

    private ImageView imageView;
    private TextView textView;

    /* Constructors */
    public TextImageButton(Context context) {
        this(context, null);
    }

    public TextImageButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TextImageButton(Context context, AttributeSet attrs, int defaultStyle) {
        // Inicialice el diseño principal con el estilo de botón del sistema
        // Esto establece los atributos en los que se puede hacer clic y el fondo del botón para que coincidan
        // el tema actual.
        super(context, attrs, android.R.attr.buttonStyle);
        imageView = new ImageView(context, attrs, defaultStyle);
        textView = new TextView(context, attrs, defaultStyle);
        //crear parámetros de diseño
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT,
                Gravity.CENTER);
        //Agregar las vistas
        this.addView(imageView, params);
        this.addView(textView, params);

        //Si hay imagen, cambie al modo de imagen
        if (imageView.getDrawable() != null) {
            textView.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
        }
    }

    /* Accessors */
    public void setText(CharSequence text) {
        //Cambiar a texto
        textView.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.GONE);
        //Aplicar texto
        textView.setText(text);
    }

    public void setImageResource(int resId) {
        //Cambiar a imagen
        textView.setVisibility(View.GONE);
        imageView.setVisibility(View.VISIBLE);
        //Aplicar imagen
        imageView.setImageResource(resId);
    }

    public void setImageDrawable(Drawable drawable) {
        //Cambiar a imagen
        textView.setVisibility(View.GONE);
        imageView.setVisibility(View.VISIBLE);
        //Aplicar imagen
        imageView.setImageDrawable(drawable);
    }
}

