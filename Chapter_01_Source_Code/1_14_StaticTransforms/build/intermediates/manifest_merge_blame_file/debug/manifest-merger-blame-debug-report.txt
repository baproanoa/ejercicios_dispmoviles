1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.examples.statictransforms"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="4"
8-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml
9        android:targetSdkVersion="23" />
9-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml
10
11    <application
11-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:6:5-27:19
12        android:debuggable="true"
13        android:icon="@drawable/ic_launcher"
13-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:8:9-45
14        android:label="@string/app_name"
14-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:7:9-41
15        android:testOnly="true"
16        android:theme="@style/AppTheme" >
16-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:9:9-40
17        <activity
17-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:10:9-17:20
18            android:name="com.examples.statictransforms.MainActivity"
18-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:11:13-41
19            android:label="@string/label_perspective" >
19-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:12:13-54
20            <intent-filter>
20-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:13:13-16:29
21                <action android:name="android.intent.action.MAIN" />
21-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:14:17-68
21-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:14:25-66
22
23                <category android:name="android.intent.category.LAUNCHER" />
23-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:15:17-76
23-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:15:27-74
24            </intent-filter>
25        </activity>
26        <activity
26-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:18:9-26:20
27            android:name="com.examples.statictransforms.ScrollActivity"
27-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:19:13-43
28            android:hardwareAccelerated="false"
28-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:21:13-48
29            android:label="@string/label_scroll" >
29-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:20:13-49
30            <intent-filter>
30-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:13:13-16:29
31                <action android:name="android.intent.action.MAIN" />
31-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:14:17-68
31-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:14:25-66
32
33                <category android:name="android.intent.category.LAUNCHER" />
33-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:15:17-76
33-->C:\Dispositivos_Proyectos\Chapter_01_Source_Code\1_14_StaticTransforms\src\main\AndroidManifest.xml:15:27-74
34            </intent-filter>
35        </activity>
36    </application>
37
38</manifest>
