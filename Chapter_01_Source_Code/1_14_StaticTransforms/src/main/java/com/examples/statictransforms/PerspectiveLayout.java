package com.examples.statictransforms;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

public class PerspectiveLayout extends LinearLayout {

    public PerspectiveLayout(Context context) {
        super(context);
        init();
    }

    public PerspectiveLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PerspectiveLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        // Habilite las transformaciones estáticas para getChildStaticTransformation()
        // será llamado para cada niño.
        setStaticTransformationsEnabled(true);
    }

    @Override
    protected boolean getChildStaticTransformation(View child, Transformation t) {
        // Borrar cualquier transformación existente
        t.clear();

        if (getOrientation() == HORIZONTAL) {
            // Escalar a los niños según la distancia desde el borde izquierdo
            float delta = 1.0f - ((float) child.getLeft() / getWidth());

            t.getMatrix().setScale(delta, delta, child.getWidth() / 2,
                    child.getHeight() / 2);
        } else {
            // Escale los niños según la distancia desde el borde superior
            float delta = 1.0f - ((float) child.getTop() / getHeight());

            t.getMatrix().setScale(delta, delta, child.getWidth() / 2,
                    child.getHeight() / 2);
            //También aplique un efecto de desvanecimiento según su ubicación
            t.setAlpha(delta);
        }
        return true;
    }
}
