package com.examples.statictransforms;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;

@SuppressLint("NewApi")
public class ScrollActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        HorizontalScrollView parentView = new HorizontalScrollView(this);
        PerspectiveScrollContentView contentView = new PerspectiveScrollContentView(this);

        // Desactive la aceleración de hardware para esta vista porque el ajuste dinámico de
        // Las transformaciones secundarias no funcionan actualmente en hardware. Tú también puedes
        // deshabilitar para toda la Actividad o Aplicación con Android: hardwareAccelerated = "false"
        // en el manifiesto, pero a menudo es mejor desactivar la aceleración en tan pocos lugares como
        // posible obtener el mejor rendimiento.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            contentView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        //Agregue un puñado de imágenes para desplazarse
        for (int i = 0; i < 20; i++) {
            ImageView iv = new ImageView(this);
            iv.setImageResource(R.drawable.ic_launcher);
            contentView.addView(iv);
        }
        //Agregar las vistas a la pantalla
        parentView.addView(contentView);
        setContentView(parentView);
    }
}
