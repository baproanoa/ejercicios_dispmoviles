package com.examples.statictransforms;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Transformation;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

public class PerspectiveScrollContentView extends LinearLayout {

    /* Factor de escala ajustable para vistas de niños */
    private static final float SCALE_FACTOR = 0.7f;
    /* Punto de anclaje para la transformación. (0,0) está arriba a la izquierda,
     * (1,1) está abajo a la derecha. Esto está configurado actualmente para
     * el medio inferior (0,5, 1)
     */
    private static final float ANCHOR_X = 0.5f;
    private static final float ANCHOR_Y = 1.0f;

    public PerspectiveScrollContentView(Context context) {
        super(context);
        init();
    }

    public PerspectiveScrollContentView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PerspectiveScrollContentView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        // Habilite las transformaciones estáticas para que getChildStaticTransformation ()
        // será llamado para cada niño.
        setStaticTransformationsEnabled(true);
    }

    /*
     * Método de utilidad para calcular la posición actual de cualquier
     * Ver en las coordenadas de la pantalla
     */
    private int getViewCenter(View view) {
        int[] childCoords = new int[2];
        view.getLocationOnScreen(childCoords);
        int childCenter = childCoords[0] + (view.getWidth() / 2);

        return childCenter;
    }

    @Override
    protected boolean getChildStaticTransformation(View child, Transformation t) {
        HorizontalScrollView scrollView = null;
        if (getParent() instanceof HorizontalScrollView) {
            scrollView = (HorizontalScrollView) getParent();
        }
        if (scrollView == null) {
            return false;
        }

        int childCenter = getViewCenter(child);
        int viewCenter = getViewCenter(scrollView);
        // Calcule la diferencia entre este niño y nuestro centro de padres
        // Eso determinará el factor de escala aplicado.
        float delta = Math.min(1.0f, Math.abs(childCenter - viewCenter)
                / (float) viewCenter);
        float scale = Math.max(0.4f, 1.0f - (SCALE_FACTOR * delta));
        float xTrans = child.getWidth() * ANCHOR_X;
        float yTrans = child.getHeight() * ANCHOR_Y;

        //Borrar cualquier transformación existente
        t.clear();
        //Establecer la transformación para la vista secundaria
        t.getMatrix().setScale(scale, scale, xTrans, yTrans);

        return true;
    }
}
