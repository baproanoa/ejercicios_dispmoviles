package com.examples.animateviewproperty;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AnimateActivity extends Activity implements View.OnClickListener {

    private View mViewToAnimate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Button button = (Button) findViewById(R.id.toggleButton);
        button.setOnClickListener(this);

        mViewToAnimate = findViewById(R.id.theView);
    }

    @Override
    public void onClick(View v) {
        if (mViewToAnimate.getAlpha() > 0f) {
            //Si la vista ya es visible, deslícela hacia la derecha
            mViewToAnimate.animate().alpha(0f).translationX(500f);
        } else {
            //Si la vista está oculta, realice un fundido in situ
            //Las animaciones de propiedades modifican la vista, por lo que
            //primero tenemos que restablecer la ubicación de la vista
            mViewToAnimate.setTranslationX(0f);
            mViewToAnimate.animate().alpha(1f);
        }
    }
}
