package com.androidrecipes.systemui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class ImmersiveStickyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

    }

    public void onToggleClick(View v) {
        // Aquí solo necesitamos ocultar la interfaz de usuario con un toque porque
        // el sistema hará que los controles reaparezcan para nosotros
        // cuando el usuario hace un deslizamiento de borde desde arriba o abajo.
        v.setSystemUiVisibility(
                /* Esta bandera le dice a Android que no cambie
                 * nuestro diseño al cambiar el tamaño de la ventana a
                 * ocultar mostrar los elementos del sistema
                 */
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                /* Esta bandera oculta la barra de estado del sistema.
                 */
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                /* Esta bandera oculta los controles en pantalla
                 */
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                /* Esta bandera le dice a los controles que permanezcan ocultos hasta
                 * el usuario los devuelve explícitamente con un gesto,
                 * y volver a ocultarlos después de un período.
                 */
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}
