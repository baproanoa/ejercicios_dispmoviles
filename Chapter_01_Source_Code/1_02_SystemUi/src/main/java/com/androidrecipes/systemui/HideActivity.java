package com.androidrecipes.systemui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class HideActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

    }

    public void onToggleClick(View v) {
      // Aquí solo necesitamos ocultar los controles con un toque porque
      // el sistema hará que los controles reaparezcan automáticamente
      // cada vez que se toca la pantalla después de ocultarlos.
        v.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}
