package com.androidrecipes.systemui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;

public class FullActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
// Solicite esta función para que la barra de acciones se oculte
        supportRequestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.main);

    }

    public void onToggleClick(View v) {
       
// Aquí solo necesitamos ocultar la interfaz de usuario con un toque porque
       
// el sistema hará que los controles reaparezcan automáticamente
        
// cada vez que se toca la pantalla después de ocultarlos. 
        v.setSystemUiVisibility(
               
/ * Esta bandera le dice a Android que no cambie
                 * nuestro diseño al cambiar el tamaño de la ventana a
                 * ocultar mostrar los elementos del sistema
                 */
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                /* Esta bandera oculta la barra de estado del sistema. Si
                 * Se solicita ACTION_BAR_OVERLAY, se ocultará
                 * la ActionBar también.
                 */
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                /* Esta bandera oculta los controles en pantalla
                 */
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}
