package com.androidrecipes.nfcbeam;

import android.app.Activity;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.nfc.NfcAdapter.OnNdefPushCompleteCallback;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TextView;

import java.util.Date;

public class NfcActivity extends Activity implements
        CreateNdefMessageCallback, OnNdefPushCompleteCallback {
    private static final String TAG = "NfcBeam";
    private NfcAdapter mNfcAdapter;
    private TextView mDisplay;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDisplay = new TextView(this);
        setContentView(mDisplay);

        // Verifique el adaptador NFC disponible
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            mDisplay.setText("NFC is not available on this device.");
        } else {
            //Registre la devolución de llamada para configurar el mensaje NDEF. Establecer esto hace
            // La inserción de datos NFC se activa mientras la actividad está en primer plano.
            mNfcAdapter.setNdefPushMessageCallback(this, this);
            // Registre la devolución de llamada para escuchar el mensaje enviado correctamente
            mNfcAdapter.setOnNdefPushCompleteCallback(this, this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Verifique si un Beam lanzó esta actividad
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
            processIntent(getIntent());
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        // onResume se llama después de esto para manejar la intención
        setIntent(intent);
    }

    void processIntent(Intent intent) {
        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        // solo un mensaje enviado durante el haz
        NdefMessage msg = (NdefMessage) rawMsgs[0];
        // el registro 0 contiene el tipo MIME, el registro 1 es el AAR, si está presente
        mDisplay.setText(new String(msg.getRecords()[0].getPayload()));
    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        String text = String.format("Sending A Message From Android Recipes at %s",
                DateFormat.getTimeFormat(this).format(new Date()));
        NdefMessage msg = new NdefMessage(NdefRecord.createMime(
                "application/com.example.androidrecipes.beamtext", text.getBytes())
                /**
                 * El registro de aplicaciones de Android (AAR) está comentado. Cuando un dispositivo
                 * recibe un empujón con un AAR, la aplicación especificada en el AAR
                 * está garantizado para funcionar. El AAR anula el sistema de envío de etiquetas.
                 * Puede volver a agregarlo para garantizar que este
                 * la actividad comienza al recibir un mensaje transmitido. Por ahora, este código
                 * utiliza el sistema de envío de etiquetas.
                 */
                //,NdefRecord.createApplicationRecord("com.examples.nfcbeam")
        );
        return msg;
    }

    @Override
    public void onNdefPushComplete(NfcEvent event) {
        //Esta devolución de llamada ocurre en un hilo de carpeta, no actualice
        // la interfaz de usuario directamente desde este método.
        Log.i(TAG, "Message Sent!");
    }
}
