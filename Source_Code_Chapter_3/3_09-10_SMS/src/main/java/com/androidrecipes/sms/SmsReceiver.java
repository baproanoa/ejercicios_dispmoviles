package com.androidrecipes.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.widget.Toast;

public class SmsReceiver extends BroadcastReceiver {
    //Dirección del dispositivo que nos gustaría escuchar (número de teléfono, código corto, etc.)
    private static final String SENDER_ADDRESS = "<593969020965>";

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();

        Object[] messages = (Object[]) bundle.get("pdus");
        SmsMessage[] sms = new SmsMessage[messages.length];
        //Crear mensajes para cada PDU entrante
        for (int n = 0; n < messages.length; n++) {
            sms[n] = SmsMessage.createFromPdu((byte[]) messages[n]);
        }
        for (SmsMessage msg : sms) {
            //Verifique si el mensaje proviene de nuestro remitente conocido
            if (TextUtils.equals(msg.getOriginatingAddress(), SENDER_ADDRESS)) {
                //Evitar que otras aplicaciones procesen este mensaje entrante
                abortBroadcast();

                //Mostrar nuestra propia notificación
                Toast.makeText(context,
                        "Received message from the mothership: "
                                + msg.getMessageBody(),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}

