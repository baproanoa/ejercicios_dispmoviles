package com.androidrecipes.sms;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;

@TargetApi(19)
public class SmsActivity extends Activity {
    //Dirección del dispositivo al que nos gustaría enviar (número de teléfono, código corto, etc.)
    private static final String RECIPIENT_ADDRESS = "<ENTER YOUR NUMBER HERE>";

    /* Cadenas de acciones personalizadas para la entrega de resultados */
    private static final String ACTION_SENT =
            "com.examples.sms.SENT";
    private static final String ACTION_DELIVERED =
            "com.examples.sms.DELIVERED";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Button sendButton = new Button(this);
        sendButton.setText("HOLAAAAAA");
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendSMS("Beam us up!");
            }
        });

        setContentView(sendButton);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Monitorear el estado de las operaciones
        registerReceiver(sent, new IntentFilter(ACTION_SENT));
        registerReceiver(delivered, new IntentFilter(ACTION_DELIVERED));
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Asegúrese de que los receptores no estén activos mientras estamos en segundo plano
        unregisterReceiver(sent);
        unregisterReceiver(delivered);
    }

    private void sendSMS(String message) {
        //Construya un PendingIntent para disparar en SMS enviados
        PendingIntent sIntent = PendingIntent.getBroadcast(
                this, 0, new Intent(ACTION_SENT), 0);
        //Construya un PendingIntent para activar la confirmación de entrega de SMS
        PendingIntent dIntent = PendingIntent.getBroadcast(
                this, 0, new Intent(ACTION_DELIVERED), 0);

        //Enviar el mensaje
        SmsManager manager = SmsManager.getDefault();
        manager.sendTextMessage(RECIPIENT_ADDRESS, null, message,
                sIntent, dIntent);
    }

    /*
     * BroadcastReceiver que está registrado para recibir eventos cuando
     * se envía un mensaje SMS; con el código de resultado
     */
    private BroadcastReceiver sent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    //Manejar enviado exitosamente
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                case SmsManager.RESULT_ERROR_NULL_PDU:
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    //Handle sent error
                    break;
            }
        }
    };

    /*
     * BroadcastReceiver que está registrado para recibir eventos cuando
     * se envía un mensaje SMS; con el código de resultado
     */
    private BroadcastReceiver delivered = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    //Manejar el éxito de la entrega
                    break;
                case Activity.RESULT_CANCELED:
                    //Manejar falla de entrega
                    break;
            }
        }
    };
}

