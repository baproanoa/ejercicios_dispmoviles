package com.androidrecipes.webview;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class MyActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WebView webview = new WebView(this);
        //Habilitar la compatibilidad con JavaScript
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadUrl("http://www.android.com/");

        setContentView(webview);
    }
}

