package com.androidrecipes.dragtouch;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnLongClickListener;
import android.widget.ImageView;

//Clase que controla el arrastre
public class DragTouchActivity extends Activity implements OnLongClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //definir las imagenes
        // setOnLongClickListener permite hacer clicks largos para poder arrastrar las imagenes
        findViewById(R.id.image1).setOnLongClickListener(this);
        findViewById(R.id.image2).setOnLongClickListener(this);
        findViewById(R.id.image3).setOnLongClickListener(this);
    }


    @Override
    public boolean onLongClick(View v) {
        //Cuando se da click se crea un objeto DragShadowBuilder
        DragShadowBuilder shadowBuilder = new DragShadowBuilder(v);
        v.startDrag(null, shadowBuilder, ((ImageView) v).getDrawable(), 0);
        return true;
    }

}