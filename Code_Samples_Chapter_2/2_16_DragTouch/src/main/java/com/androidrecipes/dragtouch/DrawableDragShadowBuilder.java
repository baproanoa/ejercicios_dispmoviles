/*
 * Copyright (C) 2011 by Mark Doffman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */
package com.androidrecipes.dragtouch;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.View.DragShadowBuilder;

//Clase que define la sombra del objeto mientras se está arrastrando

public class DrawableDragShadowBuilder extends DragShadowBuilder {
    private Drawable mDrawable; //se crea un objeto de tipo drawable

    public DrawableDragShadowBuilder(View view, Drawable drawable) {
        super(view);
        // se asigna el drawable
        mDrawable = drawable;
        //se coloca un color de filtro como opacidad que se activa mientras se arrastra el objeto
        mDrawable.setColorFilter(new PorterDuffColorFilter(Color.MAGENTA, PorterDuff.Mode.MULTIPLY)); //se define color y modo de fusion
    }

    @Override
    public void onProvideShadowMetrics(Point shadowSize, Point touchPoint) {
        // se guarda el tamaño de la sombra
        shadowSize.x = mDrawable.getIntrinsicWidth();
        shadowSize.y = mDrawable.getIntrinsicHeight();
        // se coloca la sombra en la zona donde se presiona y se la centra bajo la presión del dedo
        touchPoint.x = mDrawable.getIntrinsicWidth() / 2; //la division para 2 se hace para centrar la sombra bajo el dedo
        touchPoint.y = mDrawable.getIntrinsicHeight() / 2;

        mDrawable.setBounds(new Rect(0, 0, shadowSize.x, shadowSize.y));
    }

    @Override
    public void onDrawShadow(Canvas canvas) {
        //se dibuja el objeto drawable en el canvas
        mDrawable.draw(canvas);
    }
}
