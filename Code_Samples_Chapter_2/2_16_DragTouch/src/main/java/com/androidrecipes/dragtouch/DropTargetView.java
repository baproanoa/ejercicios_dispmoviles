package com.androidrecipes.dragtouch;

import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.DragEvent;
import android.view.View.OnDragListener;
import android.widget.ImageView;
//clase del contenedor para arrastre implementa la interface OnDragListener
public class DropTargetView extends ImageView implements OnDragListener {

    private boolean mDropped;

    public DropTargetView(Context context) {
        super(context);
        init();
    }

    public DropTargetView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DropTargetView(Context context, AttributeSet attrs, int defaultStyle) {
        super(context, attrs, defaultStyle);
        init();
    }

    private void init() {
        //colocar eventos de arrastre
        setOnDragListener(this);
    }

    @Override
    public boolean onDrag(android.view.View v, DragEvent event) {
        PropertyValuesHolder pvhX, pvhY; //se guarda la posición en x e y mientras se realiza el click largo
        switch (event.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED: //avisa a las vistas cuando un drag event comienza

                //Recoger los valor de x e y del movimiento de arrastre y lo escala cuando se mueve
                pvhX = PropertyValuesHolder.ofFloat("scaleX", 0.5f);
                pvhY = PropertyValuesHolder.ofFloat("scaleY", 0.5f);
                ObjectAnimator.ofPropertyValuesHolder(this, pvhX, pvhY).start();

                //Cuando empieza un nuevo drag event, se borra la imagen de las cajas drag, que se arrastró antes
                setImageDrawable(null);
                mDropped = false;
                break;
            case DragEvent.ACTION_DRAG_ENDED: //avisa a las vistas cuando un drag event se ha completado
                // La vista se resetea cuando un nuevo drag event empieza
                if (!mDropped) {
                    pvhX = PropertyValuesHolder.ofFloat("scaleX", 1f);
                    pvhY = PropertyValuesHolder.ofFloat("scaleY", 1f);
                    ObjectAnimator.ofPropertyValuesHolder(this, pvhX, pvhY).start();
                    mDropped = false;
                }
                break;
            case DragEvent.ACTION_DRAG_ENTERED: //avisa a la vista cuando el objeto ha sido arrastrado a su caja
                pvhX = PropertyValuesHolder.ofFloat("scaleX", 0.75f);
                pvhY = PropertyValuesHolder.ofFloat("scaleY", 0.75f);
                ObjectAnimator.ofPropertyValuesHolder(this, pvhX, pvhY).start();
                break;
            case DragEvent.ACTION_DRAG_EXITED: //avisa a la vista cuando el objeto ha sido arrastrado fuera de su caja
                //React to a drag leaving this view by returning to previous size
                pvhX = PropertyValuesHolder.ofFloat("scaleX", 0.5f);
                pvhY = PropertyValuesHolder.ofFloat("scaleY", 0.5f);
                ObjectAnimator.ofPropertyValuesHolder(this, pvhX, pvhY).start();
                break;
            case DragEvent.ACTION_DROP:
                // Realiza una animación corta  cuando se coloca el objeto
                //se reduce la vista suavemente, se usan fotogramas clave para la animación
                Keyframe frame0 = Keyframe.ofFloat(0f, 0.75f);
                Keyframe frame1 = Keyframe.ofFloat(0.5f, 0f);
                Keyframe frame2 = Keyframe.ofFloat(1f, 0.75f);
                pvhX = PropertyValuesHolder.ofKeyframe("scaleX", frame0, frame1,
                        frame2);
                pvhY = PropertyValuesHolder.ofKeyframe("scaleY", frame0, frame1,
                        frame2);
                ObjectAnimator.ofPropertyValuesHolder(this, pvhX, pvhY).start();
                //pasar la imagen del objeto hacia el drag event
                setImageDrawable((Drawable) event.getLocalState());
                mDropped = true;
                break;
            default:
                //Se ignoran todos los eventos que no sean de tipo drag
                return false;
        }

        //retorna verdadero cuando se ha realizado un drag event
        return true;
    }

}
