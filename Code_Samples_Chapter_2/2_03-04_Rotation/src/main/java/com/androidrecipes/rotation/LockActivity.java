package com.androidrecipes.rotation;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

public class LockActivity extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock);

        //se crea un boton de activacion y se lo vincula con el creado en el xml
        ToggleButton toggle = (ToggleButton) findViewById(R.id.toggleButton);
        //Se aplica un estado predeterminado de la orientación de la pantalla
        if (getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED) {
            toggle.setChecked(true);
        } else {
            toggle.setChecked(false);
        }
        //se implementa el listener del botón de activación
        toggle.setOnCheckedChangeListener(new OrientationLockListener());
    }

    //clase que controla la orientación
    private class OrientationLockListener implements CompoundButton.OnCheckedChangeListener {

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            int current = getResources().getConfiguration().orientation; //guarda la orientación actual
            if (isChecked) {
                switch (current) {
                    case Configuration.ORIENTATION_LANDSCAPE: //cambia a orientación horizontal
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                        break;
                    case Configuration.ORIENTATION_PORTRAIT://cambia a orientación vertical
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        break;
                    default:
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED); //orientación no especificada
                }
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);//orientación no especificada
            }
        }
    }

    ;

}
