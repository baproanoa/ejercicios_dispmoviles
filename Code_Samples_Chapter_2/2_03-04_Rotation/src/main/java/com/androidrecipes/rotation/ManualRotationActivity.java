package com.androidrecipes.rotation;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.EditText;

public class ManualRotationActivity extends Activity {

    //creamos los contenedores
    private EditText mEditText;
    private CheckBox mCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Se carga la vista
        loadView();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        super.onConfigurationChanged(newConfig);


        // Condicional si se verifica el checkbox
        if (mCheckBox.isChecked()) {
            final Bundle uiState = new Bundle();
            //se guarda el estado del UI
            saveState(uiState);
            // se vuelve a cargar la vista
            loadView();
            //se restaura el estado del UI
            restoreState(uiState);
        }
    }

//guardar el estado de los contenedores
    private void saveState(Bundle state) {
        state.putBoolean("checkbox", mCheckBox.isChecked());
        state.putString("text", mEditText.getText().toString());
    }

   //se restaura cualquier estado de los contenedores
    private void restoreState(Bundle state) {
        mCheckBox.setChecked(state.getBoolean("checkbox"));
        mEditText.setText(state.getString("text"));
    }

    //se carga la vista de la activity
    private void loadView() {
        setContentView(R.layout.activity_manual);

        //se resetea cada vez que una nueva vista se lanza
        mCheckBox = (CheckBox) findViewById(R.id.override);
        mEditText = (EditText) findViewById(R.id.text);
    }
}
