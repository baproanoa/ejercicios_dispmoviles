package com.androidrecipes.viewpager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListPagerAdapter extends FragmentPagerAdapter {

    private static final int ITEMS_PER_PAGE = 3;

    private List<String> mItems;

    public ListPagerAdapter(FragmentManager manager, List<String> items) {
        super(manager);
        mItems = items;
    }

    /*
     * Este método solo se llamará la primera vez que se necesite un Fragmento para esta posición.
     */
    @Override
    public Fragment getItem(int position) {
        int start = position * ITEMS_PER_PAGE;
        return ArrayListFragment.newInstance(getPageList(position), start);
    }

    @Override
    public int getCount() {
        //Obtener un número entero
        int pages = mItems.size() / ITEMS_PER_PAGE;
        // Agregue una página más para los valores restantes si el tamaño de la lista no es divisible por el tamaño de la página
        int excess = mItems.size() % ITEMS_PER_PAGE;
        if (excess > 0) {
            pages++;
        }

        return pages;
    }

    /*
     * Esto se llamará después de getItem () para nuevos Fragmentos, pero también cuando Fragments
     * más allá del límite de páginas fuera de la pantalla se vuelven a agregar; debemos asegurarnos de actualizar el
     * lista de estos elementos.
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ArrayListFragment fragment = (ArrayListFragment) super.instantiateItem(container, position);
        fragment.updateListItems(getPageList(position));
        return fragment;
    }

    /*
     * lamado por el marco cuando se llama a notifyDataSetChanged (), debemos decidir cómo
     * cada Fragmento ha cambiado para el nuevo conjunto de datos. También devolvemos POSITION_NONE si
     * Ya no se necesita un Fragmento en una posición particular, por lo que el adaptador puede
     * eliminarlo.
     */
    @Override
    public int getItemPosition(Object object) {
        ArrayListFragment fragment = (ArrayListFragment) object;
        int position = fragment.getBaseIndex() / ITEMS_PER_PAGE;
        if (position >= getCount()) {
            //Esta página ya no es necesaria
            return POSITION_NONE;
        } else {
            //Actualizar visualización de datos de fragmentos
            fragment.updateListItems(getPageList(position));

            return position;
        }
    }

    /*
     * Método auxiliar para obtener la parte de la lista general que debería
     * aplicado a un Fragmento dado
     */
    private List<String> getPageList(int position) {
        int start = position * ITEMS_PER_PAGE;
        int end = Math.min(start + ITEMS_PER_PAGE, mItems.size());
        List<String> itemPage = mItems.subList(start, end);

        return itemPage;
    }

    /*
     * Fragmento interno personalizado que muestra una sección de lista dentro
     * de ListView, y proporciona métodos externos para actualizar la lista
     */
    public static class ArrayListFragment extends Fragment {
        private ArrayList<String> mItems;
        private ArrayAdapter<String> mAdapter;
        private int mBaseIndex;

        public ArrayListFragment() {
            super();
            mItems = new ArrayList<String>();
        }

        //Los fragmentos se crean por convención utilizando un patrón de fábrica
        static ArrayListFragment newInstance(List<String> page, int baseIndex) {
            ArrayListFragment fragment = new ArrayListFragment();
            fragment.updateListItems(page);
            fragment.setBaseIndex(baseIndex);
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            // Hacer un nuevo adaptador para los elementos de la lista
            mAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1, mItems);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            //Construya y devuelva una vista de lista con nuestro adaptador adjunto
            ListView list = new ListView(getActivity());
            list.setAdapter(mAdapter);
            return list;
        }

        //Recupere el índice en la lista global donde comienza esta página

        public int getBaseIndex() {
            return mBaseIndex;
        }

        //Guarde el índice en la lista global donde comienza esta página
        public void setBaseIndex(int index) {
            mBaseIndex = index;
        }

        public void updateListItems(List<String> items) {
            mItems.clear();
            for (String piece : items) {
                mItems.add(piece);
            }

            if (mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        }
    }
}
