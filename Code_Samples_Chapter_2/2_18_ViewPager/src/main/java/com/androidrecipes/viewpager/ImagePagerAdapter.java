package com.androidrecipes.viewpager;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class ImagePagerAdapter extends PagerAdapter {
    private static final int[] IMAGES = {
            android.R.drawable.ic_menu_camera,
            android.R.drawable.ic_menu_add,
            android.R.drawable.ic_menu_delete,
            android.R.drawable.ic_menu_share,
            android.R.drawable.ic_menu_edit
    };
    private static final int[] COLORS = {
            Color.RED,
            Color.BLUE,
            Color.GREEN,
            Color.GRAY,
            Color.MAGENTA
    };
    private Context mContext;

    public ImagePagerAdapter(Context context) {
        super();
        mContext = context;
    }

    /*
     *Proporcione el número total de páginas
     */
    @Override
    public int getCount() {
        return 5;
    }

    /*
     * Anule este método si desea mostrar más de una página
     * a la vez dentro de los límites de contenido de ViewPager.
     */
    @Override
    public float getPageWidth(int position) {
        return 0.333f;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        //Cree un nuevo ImageView y agréguelo al contenedor proporcionado
        ImageView iv = new ImageView(mContext);
        // Establecer el contenido para esta posición
        iv.setImageResource(IMAGES[position]);
        iv.setBackgroundColor(COLORS[position]);
        // DEBE agregar la vista aquí, el marco no lo hará por usted
        container.addView(iv);
        //Devuelve esta vista también como el objeto clave para esta posición
        return iv;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //Elimina la vista del contenedor aquí
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        // Validar que el objeto devuelto por instantiateItem () esté asociado
        // con la vista agregada al contenedor en esa ubicación. Nuestro ejemplo usa
        // el mismo objeto en ambos lugares.
        return (view == object);
    }

}
