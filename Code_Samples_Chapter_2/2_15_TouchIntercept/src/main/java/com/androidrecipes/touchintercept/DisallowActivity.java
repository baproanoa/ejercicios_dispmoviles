package com.androidrecipes.touchintercept;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class DisallowActivity extends Activity implements
        ViewPager.OnPageChangeListener {
    private static final String[] ITEMS = {
            "Row One", "Row Two", "Row Three", "Row Four",
            "Row Five", "Row Six", "Row Seven", "Row Eight",
            "Row Nine", "Row Ten"
    };

    private ViewPager mViewPager;

    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Crear una vista de encabezado de elementos deslizantes horizontales
        mViewPager = new ViewPager(this);
        // Como encabezado ListView, ViewPager debe tener una altura fija
        mViewPager.setLayoutParams(new ListView.LayoutParams(
                ListView.LayoutParams.MATCH_PARENT,
                getResources().getDimensionPixelSize(R.dimen.header_height)));
        // Escuche los cambios de estado de paginación para deshabilitar los toques de los padres
        mViewPager.setOnPageChangeListener(this);
        mViewPager.setAdapter(new HeaderAdapter(this));

        // Crea una lista de desplazamiento vertical
        mListView = new ListView(this);
        // Agregue el buscapersonas como encabezado de la lista
        mListView.addHeaderView(mViewPager);
        // Agregar elementos de lista
        mListView.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, ITEMS));

        setContentView(mListView);
    }

    /* OnPageChangeListener Methods */

    @Override
    public void onPageScrolled(int position,
                               float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        // Mientras ViewPager se está desplazando, desactive el toque de ScrollView
        // interceptar para que no pueda tomar el control e intentar desplazarse verticalmente.
        // Esta bandera debe configurarse para cada gesto que desee anular.
        boolean isScrolling = state != ViewPager.SCROLL_STATE_IDLE;
        mListView.requestDisallowInterceptTouchEvent(isScrolling);
    }

    private static class HeaderAdapter extends PagerAdapter {
        private Context mContext;

        public HeaderAdapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public Object instantiateItem(ViewGroup container,
                                      int position) {
            // Crear una nueva vista de págin
            TextView tv = new TextView(mContext);
            tv.setText(String.format("Page %d", position + 1));
            tv.setBackgroundColor((position % 2 == 0) ? Color.RED
                    : Color.GREEN);
            tv.setGravity(Gravity.CENTER);
            tv.setTextColor(Color.BLACK);

            // Agregar como vista para esta posición y regresar como objeto para
            // esta posición
            container.addView(tv);
            return tv;
        }

        @Override
        public void destroyItem(ViewGroup container,
                                int position, Object object) {
            View page = (View) object;
            container.removeView(page);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }
    }
}
