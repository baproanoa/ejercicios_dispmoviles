package com.androidrecipes.customtouch;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;
import android.widget.Scroller;

public class PanScrollView extends FrameLayout {

    // Fling componentes
    private Scroller mScroller;
    private VelocityTracker mVelocityTracker;

    /* Posiciones del último evento de movimiento */
    private float mLastTouchX, mLastTouchY;
    /* Umbral de arrastre */
    private int mTouchSlop;
    /* Velocidad de lanzamiento */
    private int mMaximumVelocity, mMinimumVelocity;
    /* Arrastrar bloqueo */
    private boolean mDragging = false;

    public PanScrollView(Context context) {
        super(context);
        init(context);
    }

    public PanScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PanScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        mScroller = new Scroller(context);
        mVelocityTracker = VelocityTracker.obtain();
        // Obtenga las constantes del sistema para los umbrales táctiles
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        mMaximumVelocity = ViewConfiguration.get(context)
                .getScaledMaximumFlingVelocity();
        mMinimumVelocity = ViewConfiguration.get(context)
                .getScaledMinimumFlingVelocity();
    }

    /*
     * Anule las implementaciones de MeasureChild ... para garantizar que el niño
     * La vista se mide para que sea tan grande como quiera. El valor por defecto
     * La implementación obligará a algunos niños a ser tan grandes como esta vista.
     */
    @Override
    protected void measureChild(View child, int parentWidthMeasureSpec,
                                int parentHeightMeasureSpec) {
        int childWidthMeasureSpec;
        int childHeightMeasureSpec;

        childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(0,
                MeasureSpec.UNSPECIFIED);
        childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(0,
                MeasureSpec.UNSPECIFIED);

        child.measure(childWidthMeasureSpec, childHeightMeasureSpec);
    }

    @Override
    protected void measureChildWithMargins(View child,
                                           int parentWidthMeasureSpec, int widthUsed,
                                           int parentHeightMeasureSpec, int heightUsed) {
        final MarginLayoutParams lp = (MarginLayoutParams) child
                .getLayoutParams();

        final int childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(
                lp.leftMargin + lp.rightMargin, MeasureSpec.UNSPECIFIED);
        final int childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(
                lp.topMargin + lp.bottomMargin, MeasureSpec.UNSPECIFIED);

        child.measure(childWidthMeasureSpec, childHeightMeasureSpec);
    }

    @Override
    public void computeScroll() {
        if (mScroller.computeScrollOffset()) {
            // ViewGroup lo llama en el momento del dibujo. Usamos
            // este método para mantener la animación de la aventura
            // hasta su finalización.
            int oldX = getScrollX();
            int oldY = getScrollY();
            int x = mScroller.getCurrX();
            int y = mScroller.getCurrY();

            if (getChildCount() > 0) {
                View child = getChildAt(0);
                x = clamp(x, getWidth() - getPaddingRight() - getPaddingLeft(),
                        child.getWidth());
                y = clamp(y,
                        getHeight() - getPaddingBottom() - getPaddingTop(),
                        child.getHeight());
                if (x != oldX || y != oldY) {
                    scrollTo(x, y);
                }
            }

            // Sigue dibujando hasta que termine la animación.
            postInvalidate();
        }
    }

    // Anular scrollTo para realizar comprobaciones de límites en cualquier solicitud de desplazamiento
    @Override
    public void scrollTo(int x, int y) {
        // confiamos en el hecho de que View.scrollBy llama scrollTo.
        if (getChildCount() > 0) {
            View child = getChildAt(0);
            x = clamp(x, getWidth() - getPaddingRight() - getPaddingLeft(),
                    child.getWidth());
            y = clamp(y, getHeight() - getPaddingBottom() - getPaddingTop(),
                    child.getHeight());
            if (x != getScrollX() || y != getScrollY()) {
                super.scrollTo(x, y);
            }
        }
    }

    /*
     * Monitoree los eventos de contacto transmitidos a los niños e intercepte tan pronto como
     * se determina que estamos arrastrando. Esto permite que las vistas de los niños
     * recibir eventos táctiles si son interactivos (es decir, botones)
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // Detén cualquier aventura en curso
                if (!mScroller.isFinished()) {
                    mScroller.abortAnimation();
                }
                // Restablecer el rastreador de velocidad
                mVelocityTracker.clear();
                mVelocityTracker.addMovement(event);
                // Guardar el punto de contacto inicial
                mLastTouchX = event.getX();
                mLastTouchY = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                final float x = event.getX();
                final float y = event.getY();
                final int yDiff = (int) Math.abs(y - mLastTouchY);
                final int xDiff = (int) Math.abs(x - mLastTouchX);
                // Verifique que cualquiera de las diferencias sea suficiente para ser un lastre
                if (yDiff > mTouchSlop || xDiff > mTouchSlop) {
                    mDragging = true;
                    mVelocityTracker.addMovement(event);
                    // Comienza a capturar eventos nosotras mismas(femenino)
                    return true;
                }
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                mDragging = false;
                mVelocityTracker.clear();
                break;
        }

        return super.onInterceptTouchEvent(event);
    }

    /*
     * Alimente todos los eventos táctiles que recibimos al detector para su procesamiento.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mVelocityTracker.addMovement(event);

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // Ya hemos almacenado el punto inicial,
                // pero si llegamos aquí, una vista infantil no capturaba
                // el evento, así que tenemos que hacerlo.
                return true;
            case MotionEvent.ACTION_MOVE:
                final float x = event.getX();
                final float y = event.getY();
                float deltaY = mLastTouchY - y;
                float deltaX = mLastTouchX - x;
                // Compruebe si hay desperdicios en eventos directos
                if (!mDragging
                        && (Math.abs(deltaY) > mTouchSlop || Math.abs(deltaX) > mTouchSlop)) {
                    mDragging = true;
                }
                if (mDragging) {
                    // Desplazarse por la vista
                    scrollBy((int) deltaX, (int) deltaY);
                    // Actualizar el último evento de toque
                    mLastTouchX = x;
                    mLastTouchY = y;
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                mDragging = false;
                // Detén cualquier aventura en curso
                if (!mScroller.isFinished()) {
                    mScroller.abortAnimation();
                }
                break;
            case MotionEvent.ACTION_UP:
                mDragging = false;
                // Calcule la velocidad actual y comience una aventura si está por encima
                // el umbral mínimo.
                mVelocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
                int velocityX = (int) mVelocityTracker.getXVelocity();
                int velocityY = (int) mVelocityTracker.getYVelocity();
                if (Math.abs(velocityX) > mMinimumVelocity
                        || Math.abs(velocityY) > mMinimumVelocity) {
                    fling(-velocityX, -velocityY);
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    /*
     * Método de utilidad para inicializar el Scroller y comenzar a redibujar
     */
    public void fling(int velocityX, int velocityY) {
        if (getChildCount() > 0) {
            int height = getHeight() - getPaddingBottom() - getPaddingTop();
            int width = getWidth() - getPaddingLeft() - getPaddingRight();
            int bottom = getChildAt(0).getHeight();
            int right = getChildAt(0).getWidth();

            mScroller.fling(getScrollX(), getScrollY(), velocityX, velocityY,
                    0, Math.max(0, right - width), 0,
                    Math.max(0, bottom - height));

            invalidate();
        }
    }

    /*
     * Método de utilidad para ayudar en la verificación de límites
     */
    private int clamp(int n, int my, int child) {
        if (my >= child || n < 0) {
            /*
             * mi> = hijo es este caso: | --------------- yo --------------- |
             * | ------ niño ------ | o | --------------- yo --------------- |
             * | ------ niño ------ | o | --------------- yo --------------- |
             * | ------ niño ------ |
             *
             * n <0 es este caso: | ------ yo ------ | | -------- niño -------- |
             * | - mScrollX - |
             * /
            return 0;
        }
        if ((my + n) > child) {
            /*
             * 
este caso: | ------ yo ------ | | ------ niño ------ | | - mScrollX
             * - |
             * /
            regreso child - my;
        }
        return n;
    }
}
