package com.androidrecipes.customtouch;

import android.content.Context;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.widget.ImageView;

public class RotateZoomImageView extends ImageView {

    private ScaleGestureDetector mScaleDetector;
    private Matrix mImageMatrix;
    /* Last Rotation Angle */
    private int mLastAngle = 0;
    /* Pivot Point for Transforms */
    private int mPivotX, mPivotY;

    public RotateZoomImageView(Context context) {
        super(context);
        init(context);
    }

    public RotateZoomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RotateZoomImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        mScaleDetector = new ScaleGestureDetector(context, mScaleListener);

        setScaleType(ScaleType.MATRIX);
        mImageMatrix = new Matrix();
    }

    /*
     * Use onSizeChanged () para calcular valores basados ​​en el tamaño de la vista.
     * La vista no tiene tamaño durante init (), por lo que debemos esperar a que esto
     * llamar de vuelta.
     * /
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (w != oldw || h != oldh) {
            //Mover la imagen al centro de la vista
            int translateX = Math.abs(w - getDrawable().getIntrinsicWidth()) / 2;
            int translateY = Math.abs(h - getDrawable().getIntrinsicHeight()) / 2;
            mImageMatrix.setTranslate(translateX, translateY);
            setImageMatrix(mImageMatrix);
            //Obtenga el punto central para futuras transformaciones de escala y rotación
            mPivotX = w / 2;
            mPivotY = h / 2;
        }
    }

    private SimpleOnScaleGestureListener mScaleListener = new SimpleOnScaleGestureListener() {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            // ScaleGestureDetector calcula un factor de escala basado en si
            // los dedos se separan o se juntan
            float scaleFactor = detector.getScaleFactor();
            //Pasa ese factor a una escala para la imagen
            mImageMatrix.postScale(scaleFactor, scaleFactor, mPivotX, mPivotY);
            setImageMatrix(mImageMatrix);

            return true;
        }
    };

    /*
     * Opere en eventos de dos dedos para rotar la imagen.
     * Este método calcula el cambio de ángulo entre el
     * apunta y gira la imagen en consecuencia. Como usuario
     * gira sus dedos, la imagen seguirá.
     */
    private boolean doRotationEvent(MotionEvent event) {
        //Calcula el ángulo entre los dos dedos
        float deltaX = event.getX(0) - event.getX(1);
        float deltaY = event.getY(0) - event.getY(1);
        double radians = Math.atan(deltaY / deltaX);
        //Convertir a grados
        int degrees = (int) (radians * 180 / Math.PI);

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //Marque el ángulo inicial
                mLastAngle = degrees;
                break;
            case MotionEvent.ACTION_MOVE:
                // ATAN devuelve un valor convertido entre -90deg y + 90deg
                // que crea un punto cuando dos dedos están verticales donde el
                // signo de volteos de ángulo. Manejamos este caso girando una pequeña cantidad
                // (5 grados) en la dirección en la que viajábamos

                if ((degrees - mLastAngle) > 45) {
                    //Yendo en sentido antihorario a través del límite
                    mImageMatrix.postRotate(-5, mPivotX, mPivotY);
                } else if ((degrees - mLastAngle) < -45) {
                    //Yendo CW a través del límite
                    mImageMatrix.postRotate(5, mPivotX, mPivotY);
                } else {
                    //Rotación normal, gira la diferencia.
                    mImageMatrix.postRotate(degrees - mLastAngle, mPivotX, mPivotY);
                }
                //Publica la rotación en la imagen
                setImageMatrix(mImageMatrix);
                //Guardar el ángulo actual
                mLastAngle = degrees;
                break;
        }

        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            // No nos importa este evento directamente, pero declaramos
            // interés para que podamos obtener más tarde eventos multitáctiles.
            return true;
        }

        switch (event.getPointerCount()) {
            case 3:
                // Con tres dedos hacia abajo, amplía la imagen
                // usando el ScaleGestureDetector
                return mScaleDetector.onTouchEvent(event);
            case 2:
                // Con dos dedos hacia abajo, gira la imagen
                // siguiendo los dedos
                return doRotationEvent(event);
            default:
                //Ignorar este evento
                return super.onTouchEvent(event);
        }
    }
}
