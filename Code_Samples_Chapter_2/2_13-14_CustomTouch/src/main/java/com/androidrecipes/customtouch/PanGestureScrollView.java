package com.androidrecipes.customtouch;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;
import android.widget.Scroller;

public class PanGestureScrollView extends FrameLayout {

    private GestureDetector mDetector;
    private Scroller mScroller;

    /* Posiciones del último evento de movimiento * /
    flotador privado mInitialX, mInitialY;
    / * Umbral de arrastre */
    private int mTouchSlop;

    public PanGestureScrollView(Context context) {
        super(context);
        init(context);
    }

    public PanGestureScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PanGestureScrollView(Context context, AttributeSet attrs,
                                int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        mDetector = new GestureDetector(context, mListener);
        mScroller = new Scroller(context);
        // Obtenga las constantes del sistema para los umbrales táctiles
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    /*
     * Anule las implementaciones de MeasureChild ... para garantizar que el niño
     * La vista se mide para que sea tan grande como quiera. El valor por defecto
     * La implementación obligará a algunos niños a ser tan grandes como esta vista.
     */
    @Override
    protected void measureChild(View child, int parentWidthMeasureSpec,
                                int parentHeightMeasureSpec) {
        int childWidthMeasureSpec;
        int childHeightMeasureSpec;

        childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(0,
                MeasureSpec.UNSPECIFIED);
        childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(0,
                MeasureSpec.UNSPECIFIED);

        child.measure(childWidthMeasureSpec, childHeightMeasureSpec);
    }

    @Override
    protected void measureChildWithMargins(View child,
                                           int parentWidthMeasureSpec, int widthUsed,
                                           int parentHeightMeasureSpec, int heightUsed) {
        final MarginLayoutParams lp = (MarginLayoutParams) child
                .getLayoutParams();

        final int childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(
                lp.leftMargin + lp.rightMargin, MeasureSpec.UNSPECIFIED);
        final int childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(
                lp.topMargin + lp.bottomMargin, MeasureSpec.UNSPECIFIED);

        child.measure(childWidthMeasureSpec, childHeightMeasureSpec);
    }

    // Oyente para manejar todos los eventos táctiles
    private SimpleOnGestureListener mListener = new SimpleOnGestureListener() {
        public boolean onDown(MotionEvent e) {
            // Cancelar cualquier aventura en curso
            if (!mScroller.isFinished()) {
                mScroller.abortAnimation();
            }
            return true;
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {
            // Llame a un método auxiliar para iniciar la animación de desplazamiento
            fling((int) -velocityX / 3, (int) -velocityY / 3);
            return true;
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                float distanceX, float distanceY) {
            // Cualquier vista se puede desplazar simplemente llamando a su método scrollBy ()
            scrollBy((int) distanceX, (int) distanceY);
            return true;
        }
    };

    @Override
    public void computeScroll() {
        if (mScroller.computeScrollOffset()) {
            // ViewGroup lo llama en el momento del dibujo. Usamos
            // este método para mantener la animación de la aventura
            // hasta su finalización
            int oldX = getScrollX();
            int oldY = getScrollY();
            int x = mScroller.getCurrX();
            int y = mScroller.getCurrY();

            if (getChildCount() > 0) {
                View child = getChildAt(0);
                x = clamp(x, getWidth() - getPaddingRight() - getPaddingLeft(),
                        child.getWidth());
                y = clamp(y,
                        getHeight() - getPaddingBottom() - getPaddingTop(),
                        child.getHeight());
                if (x != oldX || y != oldY) {
                    scrollTo(x, y);
                }
            }

            // Sigue dibujando hasta que termine la animación.
            postInvalidate();
        }
    }

    // Anular scrollTo para realizar comprobaciones de límites en cualquier solicitud de desplazamiento
    @Override
    public void scrollTo(int x, int y) {
        // confiamos en el hecho de que View.scrollBy llama scrollTo.
        if (getChildCount() > 0) {
            View child = getChildAt(0);
            x = clamp(x, getWidth() - getPaddingRight() - getPaddingLeft(),
                    child.getWidth());
            y = clamp(y, getHeight() - getPaddingBottom() - getPaddingTop(),
                    child.getHeight());
            if (x != getScrollX() || y != getScrollY()) {
                super.scrollTo(x, y);
            }
        }
    }

    /*
     * Monitoree los eventos de contacto transmitidos a los niños e intercepte tan pronto como
     * se determina que estamos arrastrando
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mInitialX = event.getX();
                mInitialY = event.getY();
                // Alimente el evento de caída al detector para que tenga
                // contexto cuando / si comienza a arrastrar
                mDetector.onTouchEvent(event);
                break;
            case MotionEvent.ACTION_MOVE:
                final float x = event.getX();
                final float y = event.getY();
                final int yDiff = (int) Math.abs(y - mInitialY);
                final int xDiff = (int) Math.abs(x - mInitialX);
                // Verifique que cualquiera de las diferencias sea suficiente para ser un lastre
                if (yDiff > mTouchSlop || xDiff > mTouchSlop) {
                    // Start capturing events
                    return true;
                }
                break;
        }

        return super.onInterceptTouchEvent(event);
    }

    /*
     * Alimente todos los eventos táctiles que recibimos al detector para su procesamiento.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mDetector.onTouchEvent(event);
    }

    /*
     * Método de utilidad para inicializar el Scroller y comenzar a redibujar
     */
    public void fling(int velocityX, int velocityY) {
        if (getChildCount() > 0) {
            int height = getHeight() - getPaddingBottom() - getPaddingTop();
            int width = getWidth() - getPaddingLeft() - getPaddingRight();
            int bottom = getChildAt(0).getHeight();
            int right = getChildAt(0).getWidth();

            mScroller.fling(getScrollX(), getScrollY(), velocityX, velocityY,
                    0, Math.max(0, right - width), 0,
                    Math.max(0, bottom - height));

            invalidate();
        }
    }

    /*
     * Método de utilidad para ayudar en la verificación de límites
     */
    private int clamp(int n, int my, int child) {
        if (my >= child || n < 0) {
            /*
             * mi> = hijo es este caso: | --------------- yo --------------- |
             * | ------ niño ------ | o | --------------- yo --------------- |
             * | ------ niño ------ | o | --------------- yo --------------- |
             * | ------ niño ------ |
             *
             * n <0 es este caso: | ------ yo ------ | | -------- niño -------- |
             * | - mScrollX - |
             */
            return 0;
        }
        if ((my + n) > child) {
            /*
             * este caso: | ------ yo ------ | | ------ niño ------ | | - mScrollX
             * --|
             */
            return child - my;
        }
        return n;
    }
}
