package com.androidrecipes.customtouch;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

public class RemoteScrollActivity extends Activity implements View.OnTouchListener {

    private TextView mTouchText;
    private HorizontalScrollView mScrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mTouchText = (TextView) findViewById(R.id.text_touch);
        mScrollView = (HorizontalScrollView) findViewById(R.id.scroll_view);
        //Adjunte un oyente para eventos táctiles a la vista superior
        mTouchText.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // Puede masajear la ubicación del evento si es necesario.
        // Aquí establecemos la ubicación vertical para cada evento en
        // el medio de HorizontalScrollView.

        // Los eventos de la vista esperan que sean relativos a sus propias coordenadas.
        event.setLocation(event.getX(), mScrollView.getHeight() / 2);

        // Reenvíe cada evento de TextView al
        // HorizontalScrollView
        mScrollView.dispatchTouchEvent(event);
        return true;
    }
}
