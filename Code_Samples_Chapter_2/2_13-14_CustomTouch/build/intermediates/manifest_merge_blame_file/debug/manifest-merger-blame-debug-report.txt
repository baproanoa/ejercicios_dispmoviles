1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.androidrecipes.customtouch"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="9"
8-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml
9        android:targetSdkVersion="24" />
9-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml
10
11    <application
11-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:6:5-34:19
12        android:debuggable="true"
13        android:icon="@drawable/ic_launcher"
13-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:8:9-45
14        android:label="@string/app_name"
14-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:7:9-41
15        android:theme="@style/AppTheme" >
15-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:9:9-40
16        <activity android:name="com.androidrecipes.customtouch.RemoteScrollActivity" >
16-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:10:9-15:20
16-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:10:19-55
17            <intent-filter>
17-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:11:13-14:29
18                <action android:name="android.intent.action.MAIN" />
18-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:12:17-68
18-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:12:25-66
19
20                <category android:name="android.intent.category.LAUNCHER" />
20-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:13:17-76
20-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:13:27-74
21            </intent-filter>
22        </activity>
23        <activity android:name="com.androidrecipes.customtouch.DelegateActivity" >
23-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:16:9-21:20
23-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:16:19-51
24            <intent-filter>
24-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:11:13-14:29
25                <action android:name="android.intent.action.MAIN" />
25-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:12:17-68
25-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:12:25-66
26
27                <category android:name="android.intent.category.LAUNCHER" />
27-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:13:17-76
27-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:13:27-74
28            </intent-filter>
29        </activity>
30        <activity android:name="com.androidrecipes.customtouch.ImageActivity" >
30-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:22:9-27:20
30-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:22:19-48
31            <intent-filter>
31-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:11:13-14:29
32                <action android:name="android.intent.action.MAIN" />
32-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:12:17-68
32-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:12:25-66
33
34                <category android:name="android.intent.category.LAUNCHER" />
34-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:13:17-76
34-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:13:27-74
35            </intent-filter>
36        </activity>
37        <activity android:name="com.androidrecipes.customtouch.PanScrollActivity" >
37-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:28:9-33:20
37-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:28:19-52
38            <intent-filter>
38-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:11:13-14:29
39                <action android:name="android.intent.action.MAIN" />
39-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:12:17-68
39-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:12:25-66
40
41                <category android:name="android.intent.category.LAUNCHER" />
41-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:13:17-76
41-->C:\Dispositivos_Proyectos\Code_Samples_Chapter_2\2_13-14_CustomTouch\src\main\AndroidManifest.xml:13:27-74
42            </intent-filter>
43        </activity>
44    </application>
45
46</manifest>
